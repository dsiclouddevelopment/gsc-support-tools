var now = new Date(); 


function OnStart(request, response) {
	var paramsObj = getParams(request);
	//response.write(writeParams(request, request.getAllParameters()));
    var html = '';
    
    switch(paramsObj.action) {
        case "getFilters":
            html = getFilters(paramsObj);
			break;
		case "getResults":
			html = getResults(paramsObj);
			break;
        default:
            html = getFilters(paramsObj);
            break;
    }

	response.write(html);
}

function getFilters(paramsObj){
	nlapiLogExecution("DEBUG", 'getFilters', 'paramsObj = ' + JSON.stringify(paramsObj));
	var familyList = getOptionTags('customlist_dsi_family');
	var customerList = getOptionTags('customer');
	var now = new Date();
	var strToday = formatDateYMD(now);
	var strDateDefault = formatDateYMD(nlapiAddMonths(now, -24));
	var html = '', body = '';
	html += '<!DOCTYPE html>';
	html += '<html><head>';
	html += '<title>Case Search Criteria</title>';

	html += '<meta name="viewport" content="width=device-width, initial-scale=1">\n';
;

	html += '<script>\n' +
			'document.addEventListener("keypress", function(e) {\n' +
			'  if (e.charCode === 13) {\n' +
			'  e.preventDefault();\n' +
			'  document.appform.submit();\n' +
			'  }\n' +
			'});\n' +
			'</script>\n'; 

	html += '<style>' + getCSS('DSI_Suitelet.css');
	html += '';
	html += '</style>\n'

	html += '</head><body><div class=\"DSI_page\" id=\"DSI_page\">';
	html += '<div class=\"content\"> ';
	html += '<div class=\"header\">\n';
	html += '	<div class=\"DSI_navigationHeader\">';
	html += '		<div class=\"importLogoDiv\"><img src=\"' + getPageLogo() + '\" class=\"importLogo\" /></div>\n';
	html += '	</div>';
	html += '	<div class=\"DSI_appTitle\">';
	html += '		<h1 class="DSI_Header_Title">CASE SEARCH</h1>';
	html += '	</div>';
	html += '</div ><hr class=\"bottomHr\"> ';

	html += '<form name="appform" id=\"appForm\" class=\"appForm\" method="get" action="/app/site/hosting/scriptlet.nl" >\n';

	html += '<input type="hidden" name="action" value="getResults">\n';
	html += '<input type="hidden" name="script" value="customscript_dsi_case_search">\n';
	html += '<input type="hidden" name="deploy" value="customdeploy_dsi_case_search">\n';

	html += '<div class="DSI_Form_Container">\n' +

		'	<div class="oneColumn ">' +
		'			<h4 class="inputHeader">Keywords</h4> \n' +
		'			<input type="text" class="formInput" name="keywords" id="keywords"> </input>\n' +
		'	</div>\n' +

		'	<div class="">' +
		'		<h3 class="subheader">&nbsp;</h3>' +
		'	</div>\n' +

		'	<div class="">' +
		'		<h3 class="subheader">Filters</h3>' +
		'	</div>\n' +
		'<hr>\n' +

		'<div class="twoColumns">\n' +

		
/************************Can't acces due to permission restrictions */		
		//'	<div class="leftColumn ">' +
		//'		<h4 class="inputHeader">Product</h4>' +
		//'		<select class="formInput" name="productList" id="productList">' + productList + '</select>' +
		//'	</div>\n' +

		//'	<div class="rightColumn ">' +
		//'		<h4 class="inputHeader">Version</h4>' +
		//'		<select class="formInput" name="versionList" id="versionList">' + versionList + '</select>' +
		//'	</div>\n' +

		'	<div class="leftColumn ">' +
		'		<h4 class="inputHeader">Created On or After</h4>' +
		'		<input type="date" id="start" name="datestart"	value="' + strDateDefault + '"min="2015-10-01" max="' + strToday + '">' +
		
		'	</div>\n' +

		'	<div class="rightColumn ">' +
		'		<h4 class="inputHeader">Customer</h4>' +
		'		<select class="formInput" name="customer" id="customer">' + customerList + '</select>' +
		'	</div>\n' +
		

		'	<div class="leftColumn ">' +
		'		<h4 class="inputHeader">Product Family</h4>' +
		'		<select class="formInput" name="family" id="family">' + familyList + '</select>' +
		'	</div>\n' +

		'	<div class="rightColumn ">' +
		'		<h4 class="inputHeader">TFS Bug Number</h4>' +
		'			<input type="text" class="formInput" name="tfsNumber" id="tfsNumber"> </input>\n' +
		'	</div>\n' +

		'</div>\n' +
	
		'</div >\n<input type="submit" class="noShow"></form >\n</div >\n<div class="footer">\n' +
		'	<div class="oneColumn ">\n' +
		'		<input id="submitButton" class="formInput buttonStyles" ' +
		'			onclick="' +
		'				appform.submit();" ' +
		'			type="button" value="SEARCH" class="SubmitBtn">\n' +
		'		</input>\n' +
		'	</div>\n' +
		'</div ></div ></body ></html > ';

	return html;
}

function getResults(paramsObj){
	var results = getSearch(paramsObj);
	nlapiLogExecution("DEBUG", 'getResults', 'results = ' + (results?JSON.stringify(results):'null') );
//html += JSON.stringify(results);

	var html = '';
	html += '<!DOCTYPE html>\n';
	html += '<html>\n<head>\n';
	html += '<title>Case Search Results</title>\n';

	html += '<meta name="viewport" content="width=device-width, initial-scale=1">\n';
	html += '<style>' + getCSS('DSI_Suitelet.css');
	html += '\n';
	html += '</style>\n';
	html += '</head>\n<body>\n<div class=\"DSI_page\">\n';
	html += '<div class=\"content\"> \n';
	html += '<div class=\"header\">\n';
	html += '   <div class=\"DSI_navigationHeader\">\n';
	//html += '      <div class=\"appNavBackDiv\" onclick="window.history.go(-1);">' + 
	//		'      <input class="formInput buttonStyles" onclick="window.history.back();" type="button" value="BACK" class="SubmitBtn"></input>' +
	//		'      </div>\n';
	
	html += '   <div class=\"importLogoDiv\"><img src=\"' + getPageLogo() + '\" class=\"importLogo\" /></div>\n';
	html += '   </div>\n';
	html += '   <div class=\"DSI_appTitle\">\n';
	html += '      <h1 class="DSI_Header_Title">CASE SEARCH RESULTS</h1>\n';
	html += '   </div>\n';
	html += '   <div class="subheader" style="border: none;text-transform: none">' + (results? results.length: 'No') + ' cases found</div>\n' ;
	html += '</div ><hr class=\"bottomHr\"> \n';


	html += '  <br><div style=\"margin:0 10px;\">\n';
	html += '    <table name=\"DSIitemselector\" id=\"DSIitemselector\" class=\"suitelettable\">\n';
	html += buildTable(results);
	html += '    </table>\n';
	html += '  </div>\n' ;
	html += '	<div class="oneColumn ">\n' +
			'		<input class="formInput buttonStyles" ' +
			'			onclick="' +
			//'				window.history.back();" ' +
			'				window.location = \'/app/site/hosting/scriptlet.nl?script=customscript_dsi_case_search&deploy=customdeploy_dsi_case_search\';" ' +
			'			type="button" value="NEW SEARCH" class="SubmitBtn">\n' +
			'		</input>\n' +
			'	</div>\n' ;
	html += '</div >\n</body >\n</html > ';
	return html;
}

function getSearch(paramsObj){
	nlapiLogExecution("DEBUG", 'getSearch', 'BEGIN');
	//datestart format YYYY-MM-DD
	var filters = [
		["isinactive","is","F"], 
		"AND", 
		["company","isnotempty",""]
	 ]
	
	 nlapiLogExecution("DEBUG", 'getSearch', 'starting filters = ' + JSON.stringify(filters));

	var columns = [
		new nlobjSearchColumn("internalid").setSort(false), 
		new nlobjSearchColumn("title"), 
		new nlobjSearchColumn("casenumber"), 
		new nlobjSearchColumn("createddate"), 
		new nlobjSearchColumn("company"), 
		new nlobjSearchColumn("product"), 
		new nlobjSearchColumn("custevent_gsc_dispo_field"), 
		new nlobjSearchColumn("custevent_dsi_family_3"), 
		new nlobjSearchColumn("custevent_gsc_hotfix_level"),
		new nlobjSearchColumn("custevent_gsc_case_description")
	 ];
	 
	 if(paramsObj.customer){
		nlapiLogExecution("DEBUG", 'getSearch', 'customer filter');
		filters.push('AND');
		filters.push(["customer.internalid", "anyof", paramsObj.customer]);
	}

	if(paramsObj.family){
		nlapiLogExecution("DEBUG", 'getSearch', 'family filter');
		filters.push('AND');
		filters.push(["custevent_dsi_family_3", "anyof", paramsObj.family]);
	}

	if(paramsObj.tfsNumber){
		nlapiLogExecution("DEBUG", 'getSearch', 'tfsNumber filter');
		filters.push('AND');
		filters.push(["custeventgsc_dev_bug_number", "contains", paramsObj.tfsNumber]);
	}

	if(paramsObj.datestart){
		nlapiLogExecution("DEBUG", 'getSearch', 'date filter ' + paramsObj.datestart);
		var strDate = formatDateNS(paramsObj.datestart);
		nlapiLogExecution("DEBUG", 'getSearch', 'strDate = ' + strDate);
		filters.push('AND');
		filters.push(["createddate", "onorafter",strDate]);
	}

	if(paramsObj.keywords){
		nlapiLogExecution("DEBUG", 'getSearch', 'title (subject) filter');
		var keyfields = [
			["title","haskeywords",paramsObj.keywords],
			"OR",
			["quicknote","contains",paramsObj.keywords],
			"OR",
			["company","haskeywords",paramsObj.keywords],
			"OR",
			["custevent_gsc_case_description","contains",paramsObj.keywords]
		]
		filters.push('AND');
		filters.push(keyfields);
		nlapiLogExecution("DEBUG", 'getSearch', 'after keywords filters = ' + JSON.stringify(filters));
	}
	nlapiLogExecution("DEBUG", 'getSearch', 'About to search filters = ' + JSON.stringify(filters));
	var results = nlapiSearchRecord('supportcase', null, filters, columns);
	nlapiLogExecution("DEBUG", 'getSearch', 'END results = ' + (results?JSON.stringify(results):'null') );
	return results;
}

function buildTable(results) {
	nlapiLogExecution("DEBUG", 'buildTable', 'BEGIN results.length = ' + (results?results.length:'null') );
	var retval = '';
	var wideCell = ''+
	' width:170px;'+
	' height:40px;'+
	' vertical-align: middle;'+
	' overflow-y:auto;'+
	' overflow-x:hidden;'+
	' line-height:1 !important;'+
	' padding:0;'+
	' margin:5px 0;';
	

	if(results && results.length > 0){
		retval = 
	'	<tr class="DSI_tr">' +
	'    <th class="DSI_th essential" style="font-weight: bold;">Case</th>' +
	'    <th class="DSI_th essential" style="font-weight: bold;">Subject</th>' +
	'    <th class="DSI_th" style="font-weight: bold;">Date</th>' +
	'    <th class="DSI_th essential" style="font-weight: bold;">Customer</th>' +
	'    <th class="DSI_th essential" style="font-weight: bold;">Resolution</th>' +	
	'    <th class="DSI_th" style="font-weight: bold;">Disposition Code</th>' + 
	'    <th class="DSI_th" style="font-weight: bold;">Product Family</th>' +
	'    <th class="DSI_th essential" style="font-weight: bold;">Product</th>' +
	'    <th class="DSI_th" style="font-weight: bold;">Hotfix Level</th>' +
	'  </tr>\n';
		for(var i = 0; i < results.length; i++){
			var current = results[i];
			nlapiLogExecution("DEBUG", 'buildTable', 'What is current = ' + JSON.stringify(current));
			var strDate = isBlank(current.getValue('createddate')) ? '' : nlapiDateToString(new Date(current.getValue('createddate')), 'date');
			var stripe = (i%2?'row-odd':'row-even');
			retval += '\n' +
				'<tr class="DSI_tr ' + stripe + '" id="DSI_tr' + i + '" data-dsi-internalid="' + current.getId() + '" data-dsi-displayname="' + (isBlank(current.getValue('casenumber')) ? '' : current.getValue('casenumber')) + '">' +
				'	<td class="DSI_td essential ' + stripe + '"><div>' + (isBlank(current.getValue('casenumber')) ? '' : '<a target="_new" href="/app/crm/support/supportcase.nl?id=' + current.getId() + '">' + current.getValue('casenumber') + '</a>') + '</div></td>\n' +
				'	<td class="DSI_td essential ' + stripe + '"><div style="' + wideCell + '">' + (isBlank(current.getValue('title')) ? '' : current.getValue('title')) + '</div></td>\n' +
				'	<td class="DSI_td essential ' + stripe + '"><div>' + strDate + '</div></td>\n' +
				'   <td class="DSI_td essential ' + stripe + '"><div style="' + wideCell + '">' + (isBlank(current.getText('company')) ? '' : current.getText('company')) + '</div></td>\n' +
				'   <td class="DSI_td essential ' + stripe + '"><div style="' + wideCell + '">' + (isBlank(current.getValue('custevent_gsc_case_description')) ? '' : current.getValue('custevent_gsc_case_description')) + '</div></td>\n' +
				'   <td class="DSI_td essential ' + stripe + '"><div style="' + wideCell + ' width:80px;">' + (isBlank(current.getText('custevent_gsc_dispo_field')) ? '' : current.getText('custevent_gsc_dispo_field')) + '</div></td>\n' +
				'   <td class="DSI_td essential ' + stripe + '"><div>' + (isBlank(current.getText('custevent_dsi_family_3')) ? '' : current.getText('custevent_dsi_family_3')) + '</div></td>\n' +
				'   <td class="DSI_td essential ' + stripe + '"><div style="' + wideCell + '" style="width:100px;">' + (isBlank(current.getText('product')) ? '' : current.getText('product')) + '</div></td>\n' +
				'   <td class="DSI_td essential ' + stripe + '"><div>' + (isBlank(current.getValue('custevent_gsc_hotfix_level')) ? '' : current.getValue('custevent_gsc_hotfix_level')) + '</div></td>\n' + 
				'</tr>\n';
		}
	}else{
		retval += '<tr><th colspan=9>No Cases Found</th></tr>\n';
	}

	//retval += JSON.stringify(results);
	return retval;

}


function formatDateYMD(d){
	var m = (d.getMonth()+1)+'';
	var a = d.getDate()+''
	return d.getFullYear() + '-' + (m.length<2?'0'+m:m) + '-' + (a.length<2?'0'+a:a); 
}

function formatDateNS(date){ 
	//Convert html5's yyyy-mm-dd to the format in the NetSuite settings.
	//Supported NS formats 'D/M/YYYY', 'M/D/YYYY', 'MM/DD/YYYY', 'YYYY/M/D'
	nlapiLogExecution("DEBUG", 'formatDateNS', 'BEGIN');
	var retval = '';
	var format = nlapiLoadConfiguration('companypreferences').getFieldValue('dateformat');
	var M = date.substr(5,2), D = date.substr(8,2), Y =  date.substr(0,4);
	nlapiLogExecution("DEBUG", 'formatDateNS', 'format =' + format);
	switch(format) {
		case 'D/M/YYYY':
			retval = D + '/' + M + '/' + Y; 
			break;
		case 'YYYY/M/D':
			retval = date.replace('-', '/');
			break;
		case 'M/D/YYYY':
			retval = (M[0]=='0' && M.length >1? M[1]:M) + '/' + (D[0]=='0' && D.length >1? D[1]:D) + '/' + Y;
			break;
		case 'MM/DD/YYYY':
		default: // default to 'Merica!
			retval = M + '/' + D + '/' + Y;
			break;
	}
	return retval;
}

function getOptionTags(list) {
	var arrList = listNames(list);
	var retval = '<option></option>';
	for (var c = 0; c < arrList.length; c++) {
		var insertText = '<option value=' + arrList[c].internalid + '>' + arrList[c].name + '</option>';
		retval += insertText;
	}
	
	return retval;
}

function listNames(list) { //returns array of objects
	var arrOut = [], filters = [], columns = [], override = false;
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(list == 'customer'){
		filters.push(new nlobjSearchFilter('internalid', 'case', 'noneof', '@NONE@'));
		columns.push(new nlobjSearchColumn("companyname"));
	}else{
		columns.push(new nlobjSearchColumn('name'));
	}
	
	columns.push(new nlobjSearchColumn('internalid').setSort()); //long search requires sort by internalid
	//var results = nlapiSearchRecord(list, null, filters, columns);
	var results = DSI_RunLongSearch(list,null,filters,columns,override)
	if (results != null) {
		for (var a = 0; a < results.length; a++) {
			var row = results[a];
			arrOut.push({
				name: row.getValue(columns[0]),
				internalid: row.getId()
			});
		}
	}
	else {
		retval = '{"error": {"code": "RCRD_NOT_FOUND","message":  "' + getErrorMessage(22) + '"}}';
	}
	
	//sort back to alpha by name
	arrOut.sort(function(a,b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : ((b.name.toUpperCase() > a.name.toUpperCase()) ? -1 : 0);} ); 
	
	return (arrOut);
}


function getCSS() {
	var cssFilter = new nlobjSearchFilter("name", null, "is", "DSI_CSC_Suitelet.css");
	var cssFile = nlapiSearchRecord("file", null, cssFilter, null);
	var css = '';
	//nlapiLogExecution("DEBUG", "Get CSS", cssFile.length);
	if (cssFile != null) {
		var cssId = cssFile[0].getId();
		var cssObj = nlapiLoadFile(cssId);
		css = cssObj.getValue();
	}
	return css;
}
