//My additions to the JS language
String.prototype.reverse = function () {return this.split('').reverse().join(''); };  
String.prototype.replaceLast = function (what, replacement) {return this.reverse().replace(new RegExp(what.reverse()), replacement.reverse()).reverse(); };
String.prototype.left =  function (str, n){if (n <= 0){return "";}else if (n > String(str).length){return str;}else{return String(str).substring(0,n);}};
String.prototype.right = function (str, n){if (n <= 0){return "";}else if (n > String(str).length){return str;}else{var iLen = String(str).length;return String(str).substring(iLen, iLen - n);}};
(function(){var oldParseInt = parseInt;parseInt = function(){if(arguments.length == 1){return oldParseInt(arguments[0], 10);}else{return oldParseInt.apply(this, arguments);}};})();

if (!Array.prototype.find) {
	  Array.prototype.find = function(predicate) {
	    if (this === null) {
	      throw new TypeError('Array.prototype.find called on null or undefined');
	    }
	    if (typeof predicate !== 'function') {
	      throw new TypeError('predicate must be a function');
	    }
	    var list = Object(this);
	    var length = list.length >>> 0;
	    var thisArg = arguments[1];
	    var value;

	    for (var i = 0; i < length; i++) {
	      value = list[i];
	      if (predicate.call(thisArg, value, i, list)) {
	        return value;
	      }
	    }
	    return undefined;
	  };
	} 
	 



function isBlank(value){ //returns true if value is empty, all white space, or specific values.
	//return(value == null || value == '' || value == ' ' || value == 'undefined' || value == 'null' || (typeof value.trim === 'function' && value.trim() === ''));
	return(!value || value == ' ' || value == 'undefined' || value == 'null' );
}
function isblank(v){return isBlank(v);}

function isNumeric(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}
 
function nvl(value, replace){
	return((value === null) ? replace : value);
}

function bvl(value, replace){
	return((isBlank(value)) ? replace : value);
}

function myparseInt(str) //returns zero instead of NaN, base 10 only, no exponants
{
	var num = parseInt(str,10); 
	var isExp = !!(~String(str).indexOf('e') || ~String(str).indexOf('E'));
	var isHex = !!(~String(str).indexOf('x') || ~String(str).indexOf('X'));
	return (isNaN(num) || str == '0' || isBlank(str) || isExp || isHex ? 0 : num);
}

function myparseFloat(str) //returns zero instead of NaN
{
	var num = parseFloat(str);
	//return (isNaN(num) || str == '' || str == ' ' || str == null || str == '0') ? 0 : num;
	return (isNaN(num) || str == '0' || isBlank(str)) ? 0 : num;
}

function xor(a,b){ // a and b are boolean
	return ((a||b) && !(a&&b));
}

//The following getFeature functions are so I don't have to keep finding the feature id

function isAdvancedBins(){ 
	return(nlapiGetContext().getFeature('ADVBINSERIALLOTMGMT'));
}

function isBinmanagement()
{
	return(nlapiGetContext().getFeature('BINMANAGEMENT'));
}


function isFulfillCommitted(){
	return(nlapiGetContext().getPreference('FULFILLCOMMITTED') == 'COMMITTEDONLY'); 
}

function isSerializedInventory()
{
	return(nlapiGetContext().getFeature('SERIALIZEDINVENTORY'));
}

function isLotNumberedInventory()
{
	return(nlapiGetContext().getFeature('LOTNUMBEREDINVENTORY'));
}

function isMultiLocation()
{
	//return(nlapiGetContext().getSetting('FEATURE', 'LOCATIONS') == 'T');
	return(nlapiGetContext().getFeature('LOCATIONS'));  // not the same as MULTILOCINVT
}

function isMultiLocationInvt()
{
	//return(nlapiGetContext().getSetting('FEATURE', 'MULTILOCINVT') == 'T');
	return(nlapiGetContext().getFeature('MULTILOCINVT'));  
}

function isCrossSubsidiary() {
    return isOneWorld() && nlapiGetContext().getFeature('CROSSSUBSIDIARYFULFILLMENT');
}

function isOneWorld()
{
	return(nlapiGetContext().getSetting('FEATURE', 'SUBSIDIARIES') == 'T');
}

function isUOM()
{
	return(nlapiGetContext().getFeature('UNITSOFMEASURE'));  
}

function isWebStore()
{
	return(nlapiGetContext().getFeature('WEBSTORE') == "T");  
}

function isWorkOrderWIP()
{
	return(nlapiGetContext().getFeature('MFGWORKINPROCESS'));  
}

function getCompanyLogo(){  //returns internalid of the logo shown on pages inside netsuite
	//nlapiLogExecution('DEBUG', 'getCompanyLogo', 'BEGIN');
	var config = nlapiLoadConfiguration('companyinformation');
	var configObj = transformToObj(config);
	//nlapiLogExecution('DEBUG', 'getCompanyLogo', 'pagelogo = ' + configObj.pagelogo);
	return(configObj.pagelogo);
}

function getPageLogo(){ // returns the src for an HTML img tag ex: '<img src=\"' + getPageLogo() + '\" />
	var src = '';
	var logoId = getCompanyLogo();
	if(!isBlank(logoId)){
		var logoObj = nlapiLoadFile(logoId);
		src = 'data: ' + logoObj.getType() +';base64,' + logoObj.getValue();
	}
	return(src);
}

function getDSILogo() {
	var logo = '';
	var imgFilter = new nlobjSearchFilter("name", null, "is", "NSLogo2x3.png");
	var imgFile = nlapiSearchRecord("file", null, imgFilter, null);
	if (imgFile != null) {
		var logoId = imgFile[0].getId();
		var logoObj = nlapiLoadFile(logoId);
		logo = logoObj.getURL() + '';
		//nlapiLogExecution("DEBUG", "logoURL", "logoURL = " + logo);
	}
	return logo;
}

function is_array(value) {
  return !!value && //First, we ask if the value is truthy. We do this to reject null and other falsey values.
      typeof value === 'object' && //Second, we ask if the typeof value is 'object'. This will be true for objects, arrays, and (weirdly) null.
      typeof value.length === 'number' && //Third, we ask if the value has a length property that is a number. This will always be true for arrays, but usually not for objects.
      typeof value.splice === 'function' && //Fourth, we ask if the value contains a splice method. This again will be true for all arrays. 
      !(value.propertyIsEnumerable('length')); //Finally, we ask if the length property is enumerable (will length be produced by a for in loop?). That will be false for all arrays.
}

function isValidJSDate(d){return isValidDate(d)}
function isValidDate(d) {
	// only works for D/M/YYYY format
	nlapiLogExecution('DEBUG','isValidJSDate', 'BEGIN d = ' + d);
	var retval = true;
	try {
	  var d = new Date(d);
	} catch (e) {
	  retval = false;
	}
	return retval;
  }

function isValidDateFormat(d) {
	var retval = false;

	var format = nlapiLoadConfiguration('companypreferences').getFieldValue('dateformat');
	var minYear = 1900;
	var maxYear = ((new Date()).getFullYear()) + 1;
	if(!format) format = 'M/D/YYYY'; // default to 'Merica!
	if(d){
	  // adding support for "D/M/YYYY" and "YYYY/M/D" format
	  var re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
	  if(regs = d.match(re)){
		var D, M, Y;
		if(format == 'D/M/YYYY'){
		  D = regs[1];
		  M = regs[2];
		  Y = regs[3];
		}else if(format == 'M/D/YYYY' || format == 'MM/DD/YYYY'){
		  D = regs[2];
		  M = regs[1];
		  Y = regs[3];
		}else if(format == 'YYYY/M/D'){
		  D = regs[3];
		  M = regs[2];
		  Y = regs[1];
		}
  
		var validMonth = (M >= 1 || M[2] <= 12);
		var validDay = false;
		switch(M) {
		  case 2:
			validDay = (D >= 1 || D <= 29);
			break;
		  case 1:
		  case 3:
		  case 5:
		  case 7:
		  case 8:
		  case 10:
		  case 12:
			validDay = (D >= 1 || D <= 31);
			break;
		  default:
		  validDay = (D >= 1 || D <= 30);
		}
		var validYear = (Y >= minYear || Y <= maxYear );
		retval = validDay && validMonth; // && validYear;
	  }
	}
	return retval;
  }

function isIdIn(strId, arrObjects)
{
	for (var i = 0; i < arrObjects.length; i++)
	{
		if (strId == arrObjects[i].id) 
			return (true);
	}
	return (false);
}

function isValueIn(str, arr)
{
	return(~arr.indexOf(str));
}

function isValidRecordType(type){
	//nlapiLogExecution('AUDIT', 'isValidRecordType', type);
	try{
		nlapiSearchRecord(type,null,[["internalid","anyof","1"]]);
		return(true);
	}catch(e){
		return(false);
	}
}

function getParams(request) { // convert request params to javascript object
	var retVal = {}, params = request.getAllParameters();
	for (x in params) {
		if (!isBlank(x)) {
			retVal[x] = request.getParameter(x).trim();
		}
	}
	return (retVal);
}

function writeParams(request,params){
	var retVal = '';
	for(x in params){
		if (!isBlank(x)) retVal += x+'='+request.getParameter(x)+'<br/>\n';
	}
	return (retVal);
}

function stringifyFilters(objects, type){
	var arrOut = [];
	for (var i=0;i<objects.length;i++){
		var thisObject = {};
		thisObject.name = objects[i].getName();
		thisObject.join = objects[i].getJoin();
		thisObject.operator = objects[i].getOperator();
		thisObject.formula = objects[i].getFormula();
		if (type == 'column'){
			thisObject.summary = objects[i].getSummary();
			thisObject.sort = objects[i].getSort();
			thisObject.label = objects[i].getLabel();
		}else{
			thisObject.summary = objects[i].getSummaryType();
		}
		
		
		arrOut.push(thisObject);
	}
	return(JSON.stringify(arrOut));
}


function responseText(code){
	code = parseInt(code);
	switch(code){
	case 200 : return 'OK';
	case 400 : return 'Bad Request';
	case 401 : return 'Unauthorized';
	case 402 : return 'Payment Required';
	case 403 : return 'Forbidden';
	case 404 : return 'Not Found';
	case 405 : return 'Method Not Allowed';
	case 408 : return 'Request Timeout';
	case 413 : return 'Payload Too Large';
	case 414 : return 'URL Too Long';
	case 418 : return 'Im a teapot';
	case 420 : return 'Enhance Your Calm';
	case 426 : return 'Upgrade Required';
	case 429 : return 'Too Many Requests';
	case 431 : return 'Request Header Fields Too Large';
	case 500 : return 'Internal Server Error';
	case 502 : return 'Bad Gateway';
	case 503 : return 'Service Unavailable';
	case 504 : return 'Gateway Timeout';
	case 511 : return 'Authentication Required';
	case 520 : return 'Unknown HTTP Error';
	case 599 : return 'Network Connect Timeout';
	}
	return ('');
}

function isItemItem(recObj){ // takes an nlobjRecord 
	nlapiLogExecution('DEBUG', 'isItemItem', 'BEGIN ');
	var recordtype = recObj.getRecordType(); //(typeof recObj.getRecordType === 'function') ? recObj.getRecordType() : (recObj.hasOwnProperty(recordtype)?recObj.recordtype:recObj.hasOwnProperty(type) ? recObj.type : recObj.name);
	
	nlapiLogExecution('DEBUG', 'isItemItem', 'recordtype = ' + recordtype);
	switch(recordtype){
		case 'creditmemo':
		case 'vendorcredit':
		case 'vendorbill':
		case 'returnauthorization':
		case 'vendorreturnauthorization':
		case 'transferorder':
		case 'purchaseorder':
		case 'salesorder':
		case 'itemgroup':
		case 'kititem':
		case 'assemblyitem':
		case 'itemreceipt':
		case 'itemfulfillment':
			nlapiLogExecution('DEBUG', 'isItemItem', 'TRUE');
			return (true);
			break;
		default:
			nlapiLogExecution('DEBUG', 'isItemItem', 'FALSE');
			return(false);
	}
}

function findObjectInArray(strId, key, arrObjects) {  //string, string, array of objects
	//find a arrObjects[i].key matching strId Return index
	nlapiLogExecution('DEBUG', 'findObjectInArray', 'BEGIN arrObjects.length = ' + arrObjects.length + ' strId = ' + strId + ' key = ' + key );
	for (var i = 0; i < arrObjects.length; i++){
		if (strId == arrObjects[i][key]) return (i);
	}
	return (-1);
}


function findAllIdIn(strId, arrObjects)
{
	var retVal = [];
	for (var i = 0; i < arrObjects.length; i++)
	{
		if (strId == arrObjects[i].id) 
			retVal.push(i);
	}
	return retVal;
}

function findIdIn(strId, arrObjects)
{
	for (var i = 0; i < arrObjects.length; i++)
	{
		if (strId == arrObjects[i].id) 
			return (i);
	}
	return (-1);
}

function findObjByID(strId, arrObjects)
{
	for (var i = 0; i < arrObjects.length; i++)
	{
		if (strId == arrObjects[i].internalid) 
			return (i);
	}
	return (-1);
}

function randomletter(isCapital){
	var letter = '';
	if (isCapital){
		letter = String.fromCharCode(25 + Math.floor(Math.random() * 26));
	}else{
		letter = String.fromCharCode(97 + Math.floor(Math.random() * 26));
	}
	return (letter);
}

function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}

function DSI_RunLongSearch(searchtype,searchid,DOfilters,DOcolumns,override)   
//A search function to get around the 1000 result limit
//IMPORTANT: Saved Search must be sorted by internal ID to use this function unless override == true.
//searchtype: string.  The type of record of the saved search 
//searchid: int or string. The internal id (number or string) or id (string beginning with "customsearch") of the saved search
//DOfilters: array. [optional]  An optional ARRAY of filters. (Does not support single filter, must be an array)
//DOcolumns: array. [optional]  An optional ARRAY of columns. (Does not support single column, must be an array)
//override: boolean. [optional] If true, will not perform the extra search to get > 1000 results (set to true if saved search NOT sorted by internal id)
{
	if (!is_array(DOcolumns)) DOcolumns = new Array();
	if (!is_array(DOfilters)) DOfilters = new Array();
	nlapiLogExecution('DEBUG', 'DSI_RunLongSearch', 'searchid = ' + searchid + ' searchtype = ' + searchtype + ' DOfilters.length = ' + DOfilters.length + ' DOcolumns.length = ' + DOcolumns.length); 
	var results = nlapiSearchRecord(searchtype, searchid, DOfilters, DOcolumns);
	
	if (results != null && !override)
	{
		var pageLength = results.length, iteration = 1, lastIndex = pageLength-1;
		//var temprow = results[lastIndex], tempDOcolumns = temprow.getAllColumns();
		var lastId = parseInt(results[lastIndex].getId()); 
		while (pageLength == 1000 && iteration < 30 && iteration != 0){ // cap searches at 30 to avoid governance
			nlapiLogExecution('DEBUG', 'DSI_RunLongSearch', 'Appending another search' + ' pageLength = ' + pageLength); 
			var searchDOfilters = DOfilters;
			searchDOfilters.push(new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId));
			var bigResults = nlapiSearchRecord(searchtype, searchid, searchDOfilters, DOcolumns); 
			searchDOfilters.pop();
			if (bigResults == null) //first search had exactly 1000 results
			{
				//nlapiLogExecution('DEBUG', 'DSI_RunLongSearch', 'bigResults is null, we are done.');
				pageLength = 0;
			}
			else
			{
				pageLength = bigResults.length;
				//nlapiLogExecution('DEBUG', 'DSI_RunLongSearch', 'new search successful, bigResults.length = ' + pageLength + ' iteration = ' + iteration + ' lastId = ' + lastId);
				results = results.concat(bigResults);
				lastIndex = pageLength - 1;
				//temprow = bigResults[lastIndex];
				//tempDOcolumns = temprow.getAllColumns();
				lastId = parseInt(bigResults[lastIndex].getId());
				//nlapiLogExecution('DEBUG', 'after concat', 'results.length =' + results.length);
			}
			
			iteration ++;
		}
		nlapiLogExecution('DEBUG', 'DSI_RunLongSearch', 'results.length =' + results.length);
	} else {
		nlapiLogExecution('DEBUG', 'DSI_RunLongSearch', 'results is null?' + (results == null) +  ' or override is true? ' + override);
	}
	return (results);
}

//getter function to get the custom record rectype number
function getCustomRecordTypeValue(name) {
	//leverage NetSuite's URL generator to get the record type
	return getURLParameterByName('rectype', nlapiResolveURL('RECORD', name));
}

//url parser getter function
function getURLParameterByName(name, url) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(url);
	return results === null ? "" : results[1].replace(/\+/g, " ");
}

function getFileExtension(type)  //All NetSuite supported file types
{
	//nlapiLogExecution('DEBUG', 'getFileExtension', 'Type = ' + type);
	type = type.toUpperCase();
	switch(type)
	{
		case 'AUTOCAD': return 'dwg';
		case 'BMPIMAGE': return 'bmp';
		case 'CSV': return 'csv';
		case 'EXCEL': return 'xls';
		case 'FLASH': return 'swf';
		case 'GIFIMAGE': return 'gif';
		case 'GZIP': return 'gz';
		case 'HTMLDOC': return 'htm';
		case 'ICON': return 'ico';
		case 'JAVASCRIPT': return 'js';
		case 'JPGIMAGE': return 'jpg';
		case 'MESSAGERFC': return 'emi';
		case 'MP3': return 'mp3';
		case 'MPEGMOVIE': return 'mpg';
		case 'MSPROJECT': return 'mpp';
		case 'PDF': return 'pdf';
		case 'PJPGIMAGE': return 'pjpeg';
		case 'PLAINTEXT': return 'txt';
		case 'PNGIMAGE': return 'png';
		case 'POSTSCRIPT': return 'ps';
		case 'POWERPOINT': return 'ppt';
		case 'QUICKTIME': return 'mov';
		case 'RFT': return 'rft';
		case 'SMS': return 'sms';
		case 'STYLESHEET': return 'css';
		case 'TIFFIMAGE': return 'tiff';
		case 'VISIO': return 'vsd';
		case 'WORD': return 'doc';
		case 'XMLDOC': return 'xml';
		case 'ZIP': return 'zip';
		default: return 'txt';  //default to text since all the binaries are in base64 encoded text anyway
	}
}

function getFileType(ext)  //All NetSuite supported file types
{
	//nlapiLogExecution('DEBUG', 'getFileExtension', 'Type = ' + type);
	ext = ext.toLowerCase();
	switch(ext)
	{
		case 'dwg': return 'AUTOCAD';
		case 'bmp': return 'BMPIMAGE';
		case 'csv': return 'CSV';
		case 'xls': return 'EXCEL';
		case 'swf': return 'FLASH';
		case 'gif': return 'GIFIMAGE';
		case 'gz': return 'GZIP';
		case 'html':
		case 'htm': return 'HTMLDOC';
		case 'ico': return 'ICO';
		case 'js': return 'JAVASCRIPT';
		case 'jpg': return 'JPGIMAGE';
		case 'emi': return 'MESSAGERFC';
		case 'mp3': return 'MP3';
		case 'mpg': return 'MPEGMOVIE';
		case 'mpp': return 'MSPROJECT';
		case 'pdf': return 'PDF';
		case 'pjpeg': return 'PJPGIMAGE';
		case 'txt': return 'PLAINTEXT';
		case 'png': return 'PNGIMAGE';
		case 'ps': return 'POSTSCRIPT';
		case 'ppt': return 'POWERPOINT';
		case 'mov': return 'QUICKTIME';
		case 'rft': return 'RFT';
		case 'sms': return 'SMS';
		case 'css': return 'STYLESHEET';
		case 'tiff': return 'TIFFIMAGE';
		case 'vsd': return 'VISIO';
		case 'docx':
		case 'doc': return 'WORD';
		case 'xml': return 'XMLDOC';
		case 'zip': return 'ZIP';
		default: return 'PLAINTEXT';  //default to text since all the binaries are in base64 encoded text anyway
	}
}

function getFileURL(filename) { 
	// returns the URL (string) of the first matching file name or null if not found.
	// does not differentiate between multiple files with the same name.
	var retVal = null;
	if(filename){
		var results = nlapiSearchRecord('file', null, new nlobjSearchFilter('name', null, 'is', filename), null);
		if (results) {
			var file = nlapiLoadFile(results[0].getId());
			retVal = file.getURL();
		}
	}
	return retVal;
}

function getSubRecordType(type) // search result to recordtype 
{
	//nlapiLogExecution('DEBUG', 'getSubRecordType', 'Type = ' + type);
	var retval =  type;
	retval = getItemRecordType(retval);
	if (retval == type)
		retval = getTransactionRecordType(retval);
	if (retval == type)
		retval = getEntityRecordType(retval);
	
	return retval;
}

function getTransactionRecordType(type) // search result to recordtype
{
	//nlapiLogExecution('DEBUG', 'getTransactionRecordType', 'Type = ' + type);
	switch(type)
	{
		case 'SalesOrd': return 'salesorder';
		case 'VendBill': return 'vendorbill';
		case 'VendCred': return 'vendorcredit';
		case 'LiabPymt': 
		case 'VendPymt': return 'vendorpayment';
		case 'BinWksht': return 'binworksheet';
		case 'CashRfnd': return 'cashrefund';
		case 'Commissn': return 'commission';
		case 'CardChrg': return 'charge';
		case 'CustCred': return 'creditmemo';
		case 'FxReval': return 'currencyrevaluation';
		case 'CustDep': return 'customerdeposit';
		case 'DepAppl': return 'depositapplication';
		case 'ExpRept': return 'expensereport';
		case 'InvAdjst': return 'inventoryadjustment';
		case 'InvCount': return 'inventorycount';
		case 'InvReval': return 'inventorycostrevaluation';
		case 'InvDistr': return 'inventorydistribution';
		case 'InvTrnfr': return 'inventorytransfer';
		case 'InvWksht': return 'inventoryworksheet';
		case 'CustInvc': return 'invoice';
		case 'ItemRcpt': return 'itemreceipt';
		case 'itemship': 
		case 'ItemShip': return 'itemfulfillment';
		case 'Journal': return 'journalentry';
		case 'Opprtnty': return 'opportunity';
		case 'Paycheck': return 'paycheckjournal';
		case 'CustPymt': return 'paymentitem';
		case 'PurchOrd': return 'purchaseorder';
		case 'PurchCon': return 'purchasecontract';
		case 'RtnAuth': return 'returnauthorization';
		case 'TaxPymt': return 'taxpayment';
		case 'TrnfrOrd': return 'transferorder';
		case 'VendAuth': return 'vendorreturn';
		case 'Build': return 'workorderbuild';
		case 'WorkOrd': return 'workorder';
		case 'WOClose': return 'workorderclose';
		case 'WOCompl': return 'workordercompletion';
		case 'WOIssue': return 'workorderissue';
		case 'CashSale':
		case 'Check': 
		case 'Estimate': 
		case 'Transfer': 
		case 'Deposit': return type.toLowerCase();

		default: return type;
	}
}

function getItemRecordType(type) // search result to recordtype
{
	//nlapiLogExecution('DEBUG', 'getItemRecordType', 'Type = ' + type);
	switch(type)
	{
		case 'Assembly': return 'assemblyitem';
		case 'Description': return 'descriptionitem';
		case 'Discount': return 'discountitem';
		case 'DwnLdItem': return 'downloaditem';
		//case 'EndGroup': return '';
		case 'Group': return 'itemgroup';
		case 'InvtPart': return 'inventoryitem';
		case 'Kit': return 'kititem';
		case 'Markup': return 'markupitem';
		case 'NonInvtPart': return 'noninventoryitem';
		case 'OthCharge': return 'otherchargeitem';
		case 'Payment': return 'paymentitem';
		case 'Service': return 'serviceitem';
		case 'ShipItem': return 'shipitem';
		case 'Subtotal': return 'subtotalitem';
		default: return type;
	}
}

function getEntityRecordType(type) // search result to recordtype
{
	//nlapiLogExecution('DEBUG', 'getEntityRecordType', 'Type = ' + type);
	switch(type)
	{
		case 'Company': 
		case 'CustJob': return 'customer';
		case 'Contact': 
		case 'Partner': 
		case 'Vendor': 
		case 'Employee': return type.toLowerCase();
		case 'Project': return 'job';
		case 'Group': return 'entitygroup';
		default: return type;
	}
}

function getRecordMetaType(type)
{
	//nlapiLogExecution('DEBUG', 'getRecordMetaType', 'Type = ' + type);
	switch(type)
	{
		case 'task': 
		case 'manufacturingoperationtask':
		case 'projecttask' :
		case 'phonecall': 
		case 'calendarevent': 
		case 'supportcase': 
		case 'campaign': return 'crm';
		default: return getRecordSearchType(type);
	}
	return type;
}

function getRecordSearchType(type)
{
	//nlapiLogExecution('DEBUG', 'getRecordSearchType', 'Type = ' + type);
	switch(type)
	{
		case 'customer': 
		case 'contact': 
		case 'partner': 
		case 'vendor': 
		case 'employee': 
		case 'job': 
		case 'entitygroup': return 'entity';

		case 'assemblyitem':
		case 'descriptionitem':
		case 'discountitem':
		case 'downloaditem':
		case 'itemgroup':
		case 'inventoryitem':
		case 'kititem':
		case 'lotnumberedassemblyitem':
		case 'lotnumberedinventoryitem':
		case 'markupitem':
		case 'noninventoryitem':
		case 'otherchargeitem':
		case 'paymentitem':
		case 'serializedassemblyitem':
		case 'serializedinventoryitem':
		case 'serviceitem':
		case 'shipitem':
		case 'subtotalitem': return 'item';

		case 'salesorder':
		case 'customerpayment':
		case 'vendorbill':
		case 'vendorcredit':
		case 'vendorpayment':
		case 'binworksheet':
		case 'cashrefund':
		case 'commission':
		case 'charge':
		case 'creditmemo':
		case 'currencyrevaluation':
		case 'customerdeposit':
		case 'depositapplication':
		case 'expensereport':
		case 'inventoryadjustment':
		case 'inventorycount':
		case 'inventorycostrevaluation':
		case 'inventorydistribution':
		case 'inventorytransfer':
		case 'inventoryworksheet':
		case 'invoice':
		case 'itemreceipt':
		case 'itemshipment':
		case 'itemfulfillment':
		case 'journalentry':
		case 'opportunity':
		case 'paycheckjournal':
		case 'purchaseorder':
		case 'purchasecontract':
		case 'purchaserequisition':
		case 'returnauthorization':
		case 'taxpayment':
		case 'transferorder':
		case 'vendorreturnauthorization':
		case 'workorderbuild':
		case 'workorder':
		case 'workorderclose':
		case 'workordercompletion':
		case 'workorderissue':
		case 'cashsale':
		case 'check': 
		case 'estimate': 
		case 'transfer': 
		case 'deposit': return 'transaction';

		default: return type;
	}
	
	return type;
}

function getItemListId(type) //sublist name
{
	//nlapiLogExecution('DEBUG', 'getItemListId', 'Type = ' + type);
	switch(type)
	{
		case 'binworksheet':
		case 'cashrefund':
		case 'cashsale':
		case 'creditmemo':
		case 'estimate':
		case 'inventorycount':
		case 'invoice':
		case 'itemfulfillment':
		case 'itemreceipt':
		case 'itemshipment':
		case 'opportunity':
		case 'purchaseorder':
		case 'purchasecontract':
		case 'purchaserequisition':
		case 'returnauthorization':
		case 'salesorder':
		case 'transferorder':
		case 'vendorbill':
		case 'vendorcredit':
		case 'vendorreturnauthorization':
		case 'workorder':
			return 'item';
		
		case 'inventoryadjustment': 
		case 'inventorytransfer':
		case 'transfer': 
			return 'inventory';
		
		case 'assemblybuild':
		case 'workorderclose':
		case 'workordercompletion':
		case 'workorderissue':
			return 'component';

	
		default: return type;
	}
	
	return type;
}

		
		
function getItemSearchType(type)
{
	//convert recordtype to search filter
	//nlapiLogExecution('DEBUG', 'getItemSearchType', 'Type = ' + type);
	switch(type)
	{
		case 'assemblyitem': return 'Assembly';
		case 'descriptionitem': return 'Description';
		case 'discountitem': return 'Discount';
		case 'downloaditem': return 'DwnLdItem';
		//case '': return 'EndGroup';
		case 'itemgroup': return 'Group';
		case 'inventoryitem': return 'InvtPart';
		case 'kititem': return 'Kit';
		case 'markupitem': return 'Markup';
		case 'noninventoryitem': return 'NonInvtPart';
		case 'otherchargeitem': return 'OthCharge';
		case 'paymentitem': return 'Payment';
		case 'serviceitem': return 'Service';
		case 'shipitem': return 'ShipItem';
		case 'subtotalitem': return 'Subtotal';
		case 'customer': return 'CustJob';
		default: return type;
	}
}

function unSlashJSON(v) {  //internal (from script) calls only. possible security problem if external
	nlapiLogExecution('DEBUG', 'unSlashJSON', 'v = ' + v);
try{
	eval('v = '+v+';'); 
}
catch(e){
	v = {"error": {"code": "JSON_FAIL","message": ('"' + getErrorMessage(13) + '"') }}; 
}
	return v; 
}

function parseJSONfield(str) { //expects a string from a NetSuite text field containing JSON.  It gets blackslashes and extra quotes.
	return  isBlank(str) ? '{}' : (str.replace(/\\n/g, '').replace(/\\r/g, '').replace(/\\/g, '').replace(/"\{/g, '{').replace(/\}"/g, '}'));
//	return  isBlank(str) ? '{}' : (str.replace(/\\n/g, '').replace(/\\r/g, '').replace(/\\":/g, '":').replace(/{\\"/g, '{"').replace(/\\",\\"/g, '","').replace(/\\":\\"/g, '":"'));
																																					
}

function parseJSONResults(results){ // Expects nlobjSearchResult object
//Very slow and heavy
	nlapiLogExecution('DEBUG', 'parseJSONResults', 'BEGIN');
	var retArr = [];
	if(results){
		for(var i = 0; i < results.length; i++){
			var thisResult = JSON.stringify(results[i]);
			retArr.push(unSlashJSON(parseJSONfield(thisResult)));
		}
		return retArr;
	}
	return results;
}

function transformToObj(obj){  //converts a NetSuite object (e.g. nlobjRecord) to a javascript object.  On fail, returns copy of argument
	var retObj = {};
	if(typeof obj.getAllFields === 'function'){
		var fields = obj.getAllFields();
		for(var i = 0; i < fields.length; i++){
			retObj[fields[i]] = obj.getFieldValue(fields[i]);
		}
	}else{
		retObj = obj;
	}
	return retObj;
} 

function objMerge(){ 
	// Returns a new object composed of the properties/values of any number of objects sent as arguments.
	// Does not modify argument objects
	// On name collision, last value is returned.
    var retObj = {};
    for (var i = 0; i < arguments.length; i++) { //loop through the arguments
        for (var key in arguments[i]) { //loop through all the properties in each argument object
            if (arguments[i].hasOwnProperty(key)) { // Only copy direct properties, not inherited
            	retObj[key] = arguments[i][key];
            }
        }
    }
    return retObj;
}

function getInitValues(initvalues){ // takes array of {"fieldid":"","value":""}  returns hash table
	nlapiLogExecution('DEBUG', 'getInitValues', 'is_array(initvalues) = ' + is_array(initvalues));
	var retObj = {};
	if(is_array(initvalues) && initvalues.length > 0){
		for(var i = 0; i < initvalues.length; i++){
			nlapiLogExecution('DEBUG', 'getInitValues', 'initvalues[i].fieldid = ' + initvalues[i].fieldid + '. initvalues[i].value = ' + initvalues[i].value );
			if(!isBlank(initvalues[i].fieldid)){
				retObj[initvalues[i].fieldid] = initvalues[i].value;
				nlapiLogExecution('DEBUG', 'getInitValues', 'retObj = ' + JSON.stringify(retObj) );
			}
		}
	}
	nlapiLogExecution('DEBUG', 'getInitValues', 'final retObj = ' + JSON.stringify(retObj) );
	return (retObj);
}

function findMCUserField(type){
	var meta = getRecordMetaType(type);	//returns crm, entity, item, transaction, itself
	switch(meta){
		case 'crm': 		return 'custevent_dsi_mc_username';
		case 'entity': 		return 'custentity_dsi_mc_username';
		case 'item': 		return 'custitem_dsi_mc_username';
		case 'transaction': return 'custbody_dsi_mc_username';
		default:{
			if(type=='bin') 
				return 'custrecord_dsi_mc_username';
			else
				return 'custrecord_dsi_mc_user';
		}
	}
}

function setMCUserBody(recObj, mc_user){
	nlapiLogExecution('DEBUG', 'setMCUserBody', 'BEGIN mc_user = ' + mc_user);
	recObj.setFieldValue('custbody_dsi_mc_username',mc_user);
	recObj.setFieldValue('custevent_dsi_mc_username',mc_user);
	recObj.setFieldValue('custentity_dsi_mc_username',mc_user);
	recObj.setFieldValue('custitem_dsi_mc_username',mc_user);
	recObj.setFieldValue('custrecord_dsi_mc_username',mc_user);
	recObj.setFieldValue('custrecord_dsi_mc_username_bin', mc_user);
	recObj.setFieldValue('custrecord_dsi_mc_user', mc_user);
	return recObj;
}

function findPreferredBin(location, item){
	var retval = -1;

	if(!isBlank(item) && nlapiGetContext().getFeature('BINMANAGEMENT')){
		var filters = [], columns = [];
		filters.push(new nlobjSearchFilter('internalid',null,'anyof',item));
		filters.push(new nlobjSearchFilter('inventorylocation',null,'anyof',location));
		filters.push(new nlobjSearchFilter('preferredbin',null,'is','T'));
		columns.push(new nlobjSearchColumn('binnumber'));
		
		var results = nlapiSearchRecord('item',null,filters, columns);
		if(results){
			retval = results[0].getValue('binnumber') || -1;
		}
	}
	return (retval);
}


function findRecordNameById(ID,searchType,nameField,item) //item paramater is for inventorynumber searches only
{
	nlapiLogExecution('DEBUG', 'findRecordNameById', 'internalid = ' + ID + ' searchType = ' + searchType + ' nameField = ' + nameField + ' item = ' + item);
	var retval = null;
	if(!isNumeric(ID) || myparseInt(ID) == 0){ //check for NaN and finite //convert to Int for corner case where string is accidentially in exponant or hex format.
		nlapiLogExecution('ERROR', 'findRecordNamebyID', 'internal id must be numeric ' + ID);
		return null;
	}
	var filters = new Array(), columns = new Array();
	filters.push(new nlobjSearchFilter('internalid',null,'anyof',ID));
	if(!isBlank(item) && searchType == 'inventorynumber'){
		filters.push(new nlobjSearchFilter('item',null,'anyof',item.trim()));
		nlapiLogExecution('AUDIT', 'findRecordNameById', 'Added item filter: anyof ' + item.trim());
	}
	columns.push(new nlobjSearchColumn(nameField));
	var results = nlapiSearchRecord(searchType, null, filters, columns);
	if (results != null)
	{
		retval = results[0].getValue(columns[0]);
		nlapiLogExecution('DEBUG', 'findRecordNamebyID', 'Found it. ' + nameField + ' = ' + retval);
	}
	else{
		nlapiLogExecution('ERROR', 'findRecordNamebyID', 'Did not Find ' + ID + ' searchType=' + searchType + ' field=' + nameField + ' item=' + item + '. results = null');
	}

return (retval);
}

function findRecordTypeById(ID,searchType) 
{
	//nlapiLogExecution('DEBUG', 'findRecordTypeById', 'internalid = ' + ID + ' searchType = ' + searchType);
	var retval = searchType;
	//var retObj = new Object;
	var filters = new Array(), columns = new Array();
	filters.push(new nlobjSearchFilter('internalid',null,'anyof',ID));
	columns.push(new nlobjSearchColumn('type'));

	var results = nlapiSearchRecord(searchType, null, filters, columns);

	if (results != null)
	{
		if(!isBlank(results[0].getValue(columns[0])) ) 
			retval = getSubRecordType(results[0].getValue(columns[0]));
	}

return (retval);
}

function findRecordIDbyName(name,searchType,nameField,recordType,opp,item, location) //item paramater is for inventorynumber searches only
{
	nlapiLogExecution('AUDIT', 'findRecordIDbyName', 'name = ' + name + ' searchType = ' + searchType + ' nameField = ' + nameField + ' recordType = ' + recordType + ' opp = ' + opp + ' item = ' + item);
	var retval = null;
	var filters = new Array(), columns = new Array();
	var operator = isBlank(opp)? 'contains' : opp;
	if(!isBlank(name)){
		if (!isBlank(recordType)){
			if(nameField == 'entityid'){
				//filters.push(new nlobjSearchFilter('type',null,'is',recordType));
				operator = 'contains';
			}else if(nameField == 'itemid'){
				operator = 'contains';
			}else if(nameField == 'inventorynumber'){
				operator = 'is';
			}else
			{
				operator = 'startswith';
			}
			if(nameField == 'tranid') {
				filters.push(new nlobjSearchFilter('recordtype',null,'is',recordType));
			}
		}
		nlapiLogExecution('DEBUG', 'findRecordIDbyName', 'new nlobjSearchFilter(' + nameField + ',null,' + operator + ',' + name.trim() + ')' );
		filters.push(new nlobjSearchFilter(nameField,null,operator,name.trim())); //contains is slow but is needed for child items startswith and haskeywords both fail to find child itemid
		//nlapiLogExecution('DEBUG', 'findRecordIDbyName', '!isBlank(item) = ' + (!isBlank(item)) + ' searchType == inventorynumber = ' + (searchType == 'inventorynumber'));
		if(!isBlank(location)){
			filters.push(new nlobjSearchFilter('location',null,'anyof',location));
			nlapiLogExecution('DEBUG', 'findRecordIDbyName', 'Added location filter: anyof ' + location);
		}
		if(!isBlank(item) && searchType == 'inventorynumber'){
			filters.push(new nlobjSearchFilter('item',null,'anyof',item.trim()));
			nlapiLogExecution('AUDIT', 'findRecordIDbyName', 'Added item filter: anyof ' + item.trim());
		}
		columns.push(new nlobjSearchColumn(nameField));
		var results = nlapiSearchRecord(searchType, null, filters, columns);
		if (results != null)
		{
			if(results.length == 1){
				retval = results[0].getId();
				nlapiLogExecution('DEBUG', 'findRecordIDbyName', 'Found it. id = ' + retval + ' results.length = ' + results.length + ' name = ' + results[0].getValue(columns[0]));
			}else{
				for(var i = 0; i < results.length; i++){// subsidiaries are cas insensitive
					if(searchType == 'inventorynumber'){ // inventory numbers are case sensitive
						if(name == results[i].getValue(columns[0])){
							retval = results[i].getId();
						}
					} else if(searchType !== 'subsidiary'){
						if(name.toUpperCase() == results[i].getValue(columns[0]).toUpperCase()){
							retval = results[i].getId();
						}
					}else { // make case insensitive and handle child items/locations 
						var compareValue = results[i]
							.getValue(columns[0])
							.toUpperCase()
							.split(' : ')
							.pop();
						if(name.toUpperCase() === compareValue) {
							retval = results[i].getId();
						}
					}
				}
			}
		}
		if(retval == null){
			nlapiLogExecution('DEBUG', 'findRecordIDbyName', 'Did not Find ' + name +' searchType='+searchType+' field='+nameField + '. results = null');
		}
	}
return (retval);
}

function findRecordIDsbyName(name,searchType,nameField,recordType,opp,item, location) //item paramater is for inventorynumber searches only
{
	nlapiLogExecution('AUDIT', 'findRecordIDsbyName', 'name = ' + name + ' searchType = ' + searchType + ' nameField = ' + nameField + ' recordType = ' + recordType + ' opp = ' + opp );
	var retval = new Array();
	var filters = new Array(), columns = new Array();
	var operator = isBlank(opp)? 'contains' : opp;
	if(!isBlank(name)){
		if (!isBlank(recordType)){
			if(nameField == 'entityid'){
				//filters.push(new nlobjSearchFilter('type',null,'is',recordType));
				operator = 'contains';
			}else if(nameField == 'itemid'){
				operator = 'contains';
			}else if(nameField == 'inventorynumber' || nameField == 'binnumber'){
				operator = 'is';
			}else
			{
				operator = 'startswith';
			}
			if(nameField == 'tranid') {
				filters.push(new nlobjSearchFilter('recordtype',null,'is',recordType));
			}
		}
		nlapiLogExecution('DEBUG', 'findRecordIDsbyName', 'new nlobjSearchFilter(' + nameField + ',null,' + operator + ',' + name.trim() + ')' );
		filters.push(new nlobjSearchFilter(nameField,null,operator,name.trim())); //contains is slow but is needed for child items startswith and haskeywords both fail to find child itemid
		if(!isBlank(location)){
			filters.push(new nlobjSearchFilter('location',null,'anyof',location));
			nlapiLogExecution('AUDIT', 'findRecordIDsbyName', 'Added location filter: anyof ' + location);
		}
		if(!isBlank(item) && searchType == 'inventorynumber'){
			filters.push(new nlobjSearchFilter('item',null,'anyof',item.trim()));
        }
		
		columns.push(new nlobjSearchColumn(nameField));

		var results = nlapiSearchRecord(searchType, null, filters, columns);
		if (results != null)
		{
			if(results.length == 1){
				retval = results[0].getId();
				nlapiLogExecution('DEBUG', 'findRecordIDsbyName', 'Found it. id = ' + retval + ' results.length = ' + results.length + ' name = ' + results[0].getValue(columns[0]));
			}else{
				
				for(var i = 0; i < results.length; i++){
					retval.push(results[i].getId());
				}
			}
		}
		if(retval.length == 0){
			nlapiLogExecution('DEBUG', 'findRecordIDsbyName', 'Did not Find ' + name +' searchType='+searchType+' field='+nameField + '. results = null');
		}
	}
return (retval);
}


function locationNameConflictLPN(thisNumber, newLoc){
	var filters = new Array(), columns = new Array();
	filters.push(new nlobjSearchFilter('custrecord_dsi_lp_lpn',null,'is',thisNumber));
	filters.push(new nlobjSearchFilter('custrecord_dsi_lp_location',null,'anyof',newLoc));

	columns.push(new nlobjSearchColumn('internalid'));
	
	var results = nlapiSearchRecord('customrecord_dsi_license_plate', null, filters, columns);
	if(results != null){
		return true;
	}
	return false;
}

function hasData(obj){ 
//Are all the properties of an object, or members of arrays in the object, anything but empty strings?  Recursive.
//non object returns false
//	nlapiLogExecution('DEBUG','hasData', 'BEGIN ' );
	for(field in obj){
//		nlapiLogExecution('DEBUG', 'hasData', 'field = ' + field + '. obj[field] = ' + obj[field] + '. is_array(obj[field]) = ' + is_array(obj[field]) + ' typeof field = ' + typeof obj[field]);
		if(is_array(obj[field])){
			if(obj[field].length > 1 || hasData(obj[field][0])){ // not [] and not [{}]  //[{},{}] will return true!!!!
				nlapiLogExecution('DEBUG','hasData', 'TRUE ' );
				return true; //termination case - array not empty
			}
		}else if(!isBlank(obj[field]) ){
			if (typeof obj[field] === 'string' || field instanceof String  || typeof obj[field] == 'function' || typeof obj[field] == 'boolean' || typeof obj[field] == 'number') {
				nlapiLogExecution('DEBUG','hasData', 'TRUE ' );
				return true; // termination case - string not empty
			}else{ //if it's not an array or string, it's an object {}.
				return hasData(obj[field]); 
			}
		}
	}
	nlapiLogExecution('DEBUG','hasData', 'FALSE ' );
	return false; // base case - object empty {}
}

function inventoryDetailBinSearch(options, columns) {
	nlapiLogExecution('DEBUG','inventoryDetailBinSearch', 'options = ' + JSON.stringify(options) );
	var arrFilters = options.arrFilters;
//arrFilters is array of {name:'',operator:'',value:'',join:''}  " case when NOT ({item.usebins} = 'T' AND {binnumber} IS Null) then" + 
	var onhandFormula = "case" + 
		    " when {transaction.type} = 'Invoice' then case when {transaction.createdfrom} IS NULL then {itemcount} else 0 end" +
		    " when {item.usebins} = 'T' then case when {binnumber} IS Null then 0 else {itemcount} end " +
		    " when {transaction.type} = 'Item Receipt' then case when {transaction.custcol_dsi_restock} = 'F' then 0 else {itemcount} end" + 
		    " when {transaction.type} != 'Purchase Order' then" +
		        " case when {transaction.type} != 'Sales Order' then" +
		        " case when {transaction.type} != 'Transfer Order' then" +
		        " case when {transaction.type} != 'Inventory Count' then" +
		        " case when {transaction.type} != 'Return Authorization' then" +
				" case when {transaction.type} != 'Bill' then" +
				" case when {transaction.type} != '- None -' then" + // type Bill returns '- None -' from script
		        " case when {transaction.status} IS NULL then {itemcount} else " +
		            " case when {transaction.status} !='Picked' then" +
		            " case when {transaction.status} !='Packed' then {itemcount} else 0 end " + 
				"end end end end end end end end" +
		" end";
	var onhandFormulaOld = "case when {transaction.type} != 'Invoice'  then case when {transaction.type} !='Purchase Order' then case when {transaction.type} != 'Sales Order' then case when {transaction.type} !='Transfer Order' then case when {transaction.type} != 'Inventory Count' then case when {transaction.status} IS Null then {itemcount} else case when {transaction.status} !='Picked' then case when {transaction.status} !='Packed' then {itemcount} end end end else 0 end end end end end";
	var availableFormula = "case" +
		    " when {transaction.type} = 'Invoice' then case when {transaction.createdfrom} IS NULL then {itemcount} else 0 end" +
		    " when {item.usebins} = 'T' then case when {binnumber} IS Null then 0 else {itemcount} end " +
		    " when {transaction.type} = 'Item Receipt' then case when {transaction.custcol_dsi_restock} = 'F' then 0 else {itemcount} end" +
		    " when {transaction.type} != 'Purchase Order' then" +
		        " case when {transaction.type} != 'Sales Order' then" +
		            " case when {transaction.type} != 'Transfer Order' then" +
		                " case when {transaction.type} != 'Return Authorization' then" +
							" case when {transaction.type} != 'Bill' then" +
								" case when {transaction.type} != '- None -' then" + // type Bill returns '- None -' from script
									" case when {transaction.type} != 'Inventory Count' then {itemcount} else 0 end " +
								" else 0 end" +	
		                    " else 0 end" +
		                " else 0 end" +
		            " else 0 end " +
		        " else 0 end" +
		" else 0 end";
	var availableFormulaOld = "case when {transaction.type} != 'Invoice'  then case when {transaction.type} !='Purchase Order' then case when {transaction.type} != 'Sales Order' then case when {transaction.type} !='Transfer Order' then case when {transaction.type} != 'Inventory Count' then {itemcount} else 0 end end end end end";
	var filters = [];
	//filters.push(new nlobjSearchFilter("binnumber", null, "anyof", options.bins));
	if(is_array(arrFilters) && arrFilters.length > 0){
		arrFilters.forEach(function(x){
			if(x.name && x.value && !isBlank(x.value)){
				nlapiLogExecution("DEBUG", "inventoryDetailBinSearch", "filters.push(new nlobjSearchFilter(" + x.name + ", " + (!!x.join ? x.join : 'null') + ", " + (!!x.operator ? x.operator : 'is') + ", " + x.value + "));");
				filters.push(new nlobjSearchFilter(x.name, (!!x.join ? x.join : null), (!!x.operator ? x.operator : 'is'), x.value));
			}
		});
	}
	filters.push(new nlobjSearchFilter('custrecord_dsi_designated_pack_bin', 'binnumber', 'is', 'F'));

	columns.push(new nlobjSearchColumn("item", null, "GROUP"));
	columns.push(new nlobjSearchColumn("binnumber", null, "GROUP"));
	columns.push(new nlobjSearchColumn("inventorynumber", null, "GROUP").setSort(false));
	columns.push(new nlobjSearchColumn("location", null, "GROUP"));
	columns.push(new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula(availableFormula));//available  // "case when {transaction.type} != 'Invoice'  then case when {transaction.type} !='Purchase Order' then case when {transaction.type} != 'Sales Order' then case when {transaction.type} !='Transfer Order' then case when {transaction.type} != 'Inventory Count' then {itemcount} else 0 end end end end end"
	columns.push(new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula(onhandFormula));//onhand  //"case when {transaction.type} != 'Invoice'  then case when {transaction.type} !='Purchase Order' then case when {transaction.type} != 'Sales Order' then case when {transaction.type} !='Transfer Order' then case when {transaction.type} != 'Inventory Count' then case when {transaction.status} IS Null then {itemcount} else case when {transaction.status} !='Picked' then case when {transaction.status} !='Packed' then {itemcount} end end end else 0 end end end end end"
	columns.push(new nlobjSearchColumn("salesdescription", "item", "GROUP"));
	columns.push(new nlobjSearchColumn("type", "item", "GROUP"));
	if (isUOM()){
		columns.push(new nlobjSearchColumn("unitstype", "item", "GROUP"));
		columns.push(new nlobjSearchColumn("purchaseunit", "item", "GROUP"));
		columns.push(new nlobjSearchColumn("saleunit", "item", "GROUP"));
	}
	columns.push(new nlobjSearchColumn("upccode", "item", "GROUP"));
	
	if(isLotNumberedInventory()){
		columns.push(new nlobjSearchColumn("expirationdate", "inventoryNumber", "GROUP"));
	}
	

	var arrBillDetails = inventoryDetailBinSearchBill(columns, filters);
	if(arrBillDetails && arrBillDetails.length > 0){
		filters.push(new nlobjSearchFilter('internalid', null, 'noneof', arrBillDetails));
	}

	var results = nlapiSearchRecord("inventorydetail", null, filters, columns);
	nlapiLogExecution("DEBUG", "inventoryDetailBinSearch", "filters = " + JSON.stringify(filters) + "; options: " + JSON.stringify(options));
	nlapiLogExecution("DEBUG", "inventoryDetailBinSearch", "results = " + JSON.stringify(results));
	return results;
}

function inventoryDetailBinSearchBill(columns, filters) {
	nlapiLogExecution('DEBUG','inventoryDetailBinSearchBill', 'BEGIN' );
	var retval = [];
	columns.push(new nlobjSearchColumn("formulatext", null, "GROUP").setFormula("{transaction.type}"));
	columns.push(new nlobjSearchColumn("internalid",null,"GROUP"));
	var billresults = nlapiSearchRecord("inventorydetail", null, filters, columns);
	
	for(var i = 0; billresults && i < billresults.length; i++){
		var type = billresults[i].getValue("formulatext", null, "GROUP");
		if(billresults[i].getValue("formulatext", null, "GROUP") == "Bill" || billresults[i].getValue("formulatext", null, "GROUP") ==  "- None -"){
			var id = billresults[i].getValue("internalid", null, "GROUP");
			if(id) retval.push(id);
		}
	}
	columns.pop();
	columns.pop();

	// nlapiLogExecution("DEBUG", "inventoryDetailBinSearchBill", "billresults = " + JSON.stringify(billresults) + ' retval = ' + JSON.stringify(retval) );
	nlapiLogExecution("DEBUG", "inventoryDetailBinSearchBill", 'retval = ' + JSON.stringify(retval) );
	return retval;
}

function setInvDetail(group, inventorydetail, recObj, recordtype, subrecord){
//This function is ony for sublist inventory details, not body level.
	nlapiLogExecution('DEBUG','setInvDetail', 'BEGIN group = ' + group + ' subrecord = ' + subrecord );
	if(isBlank(group)) return setBodyInvDetail(inventorydetail, recObj, recordtype, subrecord);
//IMPORTANT recObj must be loaded in dynamic mode
/*	
	STRUCTURE inventorydetail OBJECT
"inventorydetail": {
	"item":"",
	"location":"",
	"tolocation":"",
	"quantity": "",
	"unit": "",
	"inventoryassignment":	[{
		"binnumber": "",
		"binid": "",
		"tobinnumber": "",
		"tobinid": "",
		"quantity": "",
		"inventorynumber": "",
		"inventorynumberid": "",
		"expirationdate":""
	}]
}
*/	

	if(hasData(inventorydetail)){
		
		//var qty = '';
		//var recordtype = recObj.getFieldValue('recordtype');
		nlapiLogExecution('DEBUG','setInvDetail', 'recordtype = ' + recordtype);
		if(subrecord == 'countdetail')
			throw nlapiCreateError("FEATURE_NOT_AVAILABLE", getErrorMessage(43), true);

		if(isBlank(subrecord) ){
			if(recordtype == 'inventorycount'){
				subrecord = 'countdetail';
				//NetSuite just won't allow remote cycle count entry with advanced bin/serial/lot numbers
				throw nlapiCreateError("FEATURE_NOT_AVAILABLE", getErrorMessage(43), true);
			} else if(recordtype == 'workorderissue' || recordtype == 'workordercompletion' || recordtype == 'assemblybuild' || recordtype == 'assemblyunbuild'){
				subrecord = 'componentinventorydetail';
			}
			else{
				subrecord = 'inventorydetail';
			}
		}
		nlapiLogExecution('DEBUG','setInvDetail', 'group = ' + group + ' subrecord = ' + subrecord);
		
		var lastAssignmentLine = 0,invDetailObj = null;
		nlapiLogExecution('AUDIT','setInvDetail','about to view subrecord '  + subrecord);
		var viewInvDetailObj = recObj.viewCurrentLineItemSubrecord(group, subrecord);
		nlapiLogExecution('DEBUG','setInvDetail', 'viewInvDetailObj != null? ' + (viewInvDetailObj != null) );
		if(viewInvDetailObj != null){
			lastAssignmentLine = viewInvDetailObj.getLineItemCount('inventoryassignment');
			nlapiLogExecution('AUDIT','setInvDetail', subrecord + ' already exists with ' + lastAssignmentLine + ' inventoryassignment lines.');
			invDetailObj = recObj.editCurrentLineItemSubrecord(group, subrecord);
		}else{
			nlapiLogExecution('DEBUG','setInvDetail', 'creating new subrecord ' );
			invDetailObj = recObj.createCurrentLineItemSubrecord(group, subrecord);
		}
		//var invDetailObj = recObj.createCurrentLineItemSubrecord(group, subrecord);
		
		nlapiLogExecution('DEBUG','setInvDetail', 'invDetailObj == null ' + (invDetailObj == null) + ' quantity = ' + inventorydetail.quantity);
		//***************SUBRECORD'S BODY LEVEL***************/
		//if(!isBlank(inventorydetail.item)) invDetailObj.setFieldValue('item', inventorydetail.item);
		if(!isBlank(inventorydetail.location)) invDetailObj.setFieldValue('location', inventorydetail.location); 
		if(!isBlank(inventorydetail.tolocation)) invDetailObj.setFieldValue('tolocation', inventorydetail.tolocation); 
		if(!isBlank(inventorydetail.quantity)){ 
			invDetailObj.setFieldValue('quantity', inventorydetail.quantity); 
			nlapiLogExecution('DEBUG','setInvDetail', 'Just set ' + subrecord + ' quantity (inventory detail body) to ' + invDetailObj.getFieldValue('quantity') );
		}
		if(!isBlank(inventorydetail.unit)) invDetailObj.setFieldValue('unit', inventorydetail.unit);

		/*********************MISC BODY FIELDS*******************************/
		/*****************TO SUPPORT SUBRECORDS OTHER THAN INVENTORY DETAILS *************************/
		if(hasData(inventorydetail.nvpair) && is_array(inventorydetail.nvpair) ){
			nlapiLogExecution('DEBUG','setInvDetail', 'Adding nvpair to subrecord ' + subrecord);
			for(var n = 0; n < inventorydetail.nvpair.length; n++){
				if(!isBlank(inventorydetail.nvpair[n])) {
					invDetailObj.setFieldValue(inventorydetail.nvpair[n].columnid, inventorydetail.nvpair[n].newvalue);
					nlapiLogExecution('DEBUG','setInvDetail', 'Just set ' + inventorydetail.nvpair[n].columnid + ' to ' + inventorydetail.nvpair[n].newvalue);
				}
			}
		}
		
		//************inventoryassignment SUBLIST**********************/

		var arrInventoryAssignment = inventorydetail.inventoryassignment;
		if(is_array(arrInventoryAssignment)){

			for(var i = 0; i < arrInventoryAssignment.length; i++){
				nlapiLogExecution('DEBUG','setInvDetail', 'Start arrInventoryAssignment[' + i + '] This is inventoryassignment line ' + (i+1) + ' of ' + arrInventoryAssignment.length + ' Existing lines, ' + invDetailObj.getLineItemCount('inventoryassignment'));
				var thisItem = invDetailObj.getFieldValue('item');
				if( !isBlank(arrInventoryAssignment[i].line) && !isNaN(parseInt(arrInventoryAssignment[i].line)) ) {
					invDetailObj.selectLineItem('inventoryassignment', arrInventoryAssignment[i].line);
				} else if(i == 0 && recordtype == 'itemfulfillment' && invDetailObj.getLineItemCount('inventoryassignment') == '1'){
					invDetailObj.selectLineItem('inventoryassignment', '1');
				} else{
					invDetailObj.selectNewLineItem('inventoryassignment');
					invDetailObj.setFieldValue('tolocation', inventorydetail.tolocation); 
				}
				
				if(!isBlank(arrInventoryAssignment[i].quantity)){  // total of these quantities must equal line quantity
					nlapiLogExecution('DEBUG','setInvDetail', 'Setting ' + subrecord + '.inventoryassignment[' + i + '].quantity to ' + arrInventoryAssignment[i].quantity);
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'quantity', arrInventoryAssignment[i].quantity);
					nlapiLogExecution('DEBUG','setInvDetail', subrecord + '.inventoryassignment[' + i + '].quantity set to ' + invDetailObj.getCurrentLineItemValue('inventoryassignment', 'quantity') );
				}

				//Lot number or Serial number
				nlapiLogExecution('DEBUG','setInvDetail', 'isBlank(arrInventoryAssignment[i].inventorynumberid) ? ' + isBlank(arrInventoryAssignment[i].inventorynumberid) + ' isBlank(arrInventoryAssignment[i].inventorynumber) ? ' + isBlank(arrInventoryAssignment[i].inventorynumber));
				if(!isBlank(arrInventoryAssignment[i].inventorynumberid) || !isBlank(arrInventoryAssignment[i].inventorynumber)){
					var inventorynumber = arrInventoryAssignment[i].inventorynumber;
					if(isBlank(inventorynumber)){
						nlapiLogExecution('DEBUG','setInvDetail', subrecord + '.inventoryassignment[' + i + '].inventorynumber is blank. Looking up internalid, ' + arrInventoryAssignment[i].inventorynumberid + '.');
						inventorynumber = findRecordNameById(arrInventoryAssignment[i].inventorynumberid,'inventorynumber','inventorynumber', thisItem);
					}
					var inventorynumberId = arrInventoryAssignment[i].inventorynumberid;
					if(isBlank(inventorynumberId)){
						nlapiLogExecution('DEBUG','setInvDetail', subrecord + '.inventoryassignment[' + i + '].inventorynumberid is blank. Looking up inventorynumber, ' + arrInventoryAssignment[i].inventorynumber + '.');
						inventorynumberId = findRecordIDbyName(arrInventoryAssignment[i].inventorynumber,'inventorynumber','inventorynumber',null,'is',thisItem);
					}

	//JUST SET EVERY VERSION OF THE INVENTORY NUMBER. EACH FLIPPING RECORD TYPE USES IT DIFFERENTLY
					nlapiLogExecution('DEBUG', 'setInvDetail', 'inventorynumber = ' + inventorynumber + ' inventorynumberId = ' + inventorynumberId);
					if(inventorynumber != null){
						invDetailObj.setCurrentLineItemValue('inventoryassignment', 'inventorynumber', inventorynumber);
						invDetailObj.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', inventorynumber);
					}
					if(inventorynumberId != null){
						invDetailObj.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', inventorynumberId);	
					}

					
				} 
				

				//Bin
				//FROM or CURRENT
				if(!isBlank(arrInventoryAssignment[i].binid) || !isBlank(arrInventoryAssignment[i].binnumber)){
					
					var bin = arrInventoryAssignment[i].binid;
					if(isBlank(bin)){
						if(!isBlank(arrInventoryAssignment[i].binnumber)){
							bin = findRecordIDbyName(arrInventoryAssignment[i].binnumber,'bin','binnumber','bin');
						}
					}
					nlapiLogExecution('DEBUG', 'setInvDetail', 'from bin = ' + bin);
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'binnumber', bin);
				}
				//TO
				if(!isBlank(arrInventoryAssignment[i].tobinid) || !isBlank(arrInventoryAssignment[i].tobinnumber)){	
					var tobin = arrInventoryAssignment[i].tobinid;
					if(isBlank(tobin)){
						if(!isBlank(arrInventoryAssignment[i].tobinnumber)){
							tobin = findRecordIDbyName(arrInventoryAssignment[i].tobinnumber,'bin','binnumber','bin');
						}
					}
					nlapiLogExecution('DEBUG', 'setInvDetail', 'to bin = ' + tobin);
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'tobinnumber', tobin);
				}
				
				//EXPIRATION DATE (for lots)
				if(isLotNumberedInventory() && !isBlank(arrInventoryAssignment[i].expirationdate)){
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'expirationdate', arrInventoryAssignment[i].expirationdate);
				}else{
					nlapiLogExecution('DEBUG', 'setInvDetail', 'BLANK EXPIRATION');
					if(isLotNumberedInventory() && (recordtype == 'itemreceipt' || recordtype == 'workordercompletion' || recordtype == 'assemblybuild')){
						var thisItem = invDetailObj.getFieldValue('item');
						var itemType = findRecordTypeById(thisItem,'item');
						var itemFields = nlapiLookupField(itemType, thisItem, ['custitem_dsi_shelf_life','islotitem']);
						nlapiLogExecution('DEBUG', 'setInvDetail', 'thisItem = ' + thisItem + ' itemType = ' + itemType + ' islotitem = ' + itemFields['islotitem'] );
						if(itemFields['islotitem'] == 'T' && !isBlank(itemFields['custitem_dsi_shelf_life'])){
							var now = new Date(), then = new Date();;
							nlapiLogExecution('AUDIT', 'setInvDetail', 'Auto generating expiration date ' + itemFields['custitem_dsi_shelf_life'] + ' days after today.');
							if(!isBlank(itemFields['custitem_dsi_shelf_life'])){
								then.setTime(now.getTime()+86400000*itemFields['custitem_dsi_shelf_life']);
								nlapiLogExecution('DEBUG', 'setInvDetail ' + subrecord, 'shelf life = ' + itemFields['custitem_dsi_shelf_life'] + ' new expiration = ' + then.toGMTString());
								invDetailObj.setCurrentLineItemValue('inventoryassignment', 'expirationdate', nlapiDateToString(then) ) ;
							}
						}
					}
				}

				nlapiLogExecution('DEBUG', 'setInvDetail', 'Committing inventoryassignment line item ' + (i+1));
				invDetailObj.commitLineItem('inventoryassignment');
			}
		}
		nlapiLogExecution('DEBUG', 'setInvDetail', 'Committing invDetailObj ' + subrecord);
		invDetailObj.commit();
	}else{
		nlapiLogExecution('DEBUG', 'setInvDetail', 'inventorydetail has no data ');
	}
	return(recObj);
}



function setBodyInvDetail(inventorydetail, recObj, recordtype, subrecord){
	nlapiLogExecution('DEBUG','setBodyInvDetail', 'BEGIN recordtype = ' + recordtype + ' subrecord = ' + subrecord );

	if(hasData(inventorydetail)){
		//var qty = '';
		//var recordtype = recObj.getRecordType();
		nlapiLogExecution('DEBUG','setBodyInvDetail', 'recordtype = ' + recordtype);
		if(subrecord == 'countdetail')
			throw nlapiCreateError("FEATURE_NOT_AVAILABLE", getErrorMessage(43), true);

		if(isBlank(subrecord) ){
			if(recordtype == 'inventorycount'){
				subrecord = 'countdetail';
				//NetSuite just won't allow remote cycle count entry with advanced bin/serial/lot numbers
				throw nlapiCreateError("FEATURE_NOT_AVAILABLE", getErrorMessage(43), true);
			}else{
				subrecord = 'inventorydetail';
			}
		}
		nlapiLogExecution('DEBUG','setBodyInvDetail', 'subrecord = ' + subrecord);
		
		var lastAssignmentLine = 0;
		var viewInvDetailObj = recObj.viewSubrecord(subrecord);
		nlapiLogExecution('DEBUG','setBodyInvDetail', 'viewInvDetailObj != null? ' + (viewInvDetailObj != null) );
		var invDetailObj = null;
		if(viewInvDetailObj != null){
			lastAssignmentLine = viewInvDetailObj.getLineItemCount('inventoryassignment');
			nlapiLogExecution('AUDIT','setBodyInvDetail', subrecord + ' already exists with ' + lastAssignmentLine + ' inventoryassignment lines.');
			invDetailObj = recObj.editSubrecord(subrecord);
		}else{
			nlapiLogExecution('DEBUG','setBodyInvDetail', 'About to call: recObj.createSubrecord(' + subrecord + ');' );
			invDetailObj = recObj.createSubrecord(subrecord);
			//invDetailObj.setFieldValue('location', null); // to clear prefilled inventory details
		}
		
		nlapiLogExecution('DEBUG','setBodyInvDetail', 'invDetailObj == null ' + (invDetailObj == null) + ' quantity = ' + inventorydetail.quantity);
		//***************SUBRECORD'S BODY LEVEL***************/
		if(!isBlank(inventorydetail.item)) invDetailObj.setFieldValue('item', inventorydetail.item);
		if(!isBlank(inventorydetail.location)) invDetailObj.setFieldValue('location', inventorydetail.location); 
		if(!isBlank(inventorydetail.tolocation)) invDetailObj.setFieldValue('tolocation', inventorydetail.tolocation); 
		if(!isBlank(inventorydetail.quantity)) invDetailObj.setFieldValue('quantity', inventorydetail.quantity); 
	nlapiLogExecution('DEBUG','setBodyInvDetail', 'Just set ' + subrecord + ' quantity (body) to ' + invDetailObj.getFieldValue('quantity') );
		if(!isBlank(inventorydetail.unit)) invDetailObj.setFieldValue('unit', inventorydetail.unit);

		
		//************inventoryassignment SUBLIST**********************/

		var arrInventoryAssignment = inventorydetail.inventoryassignment;
		if(is_array(arrInventoryAssignment)){

			for(var i = 0; i < arrInventoryAssignment.length; i++){
				nlapiLogExecution('DEBUG','setBodyInvDetail', 'Start arrInventoryAssignment[' + i + '] This is inventoryassignment line ' + (i+1) + ' of ' + arrInventoryAssignment.length);
				var thisItem = invDetailObj.getFieldValue('item');
				//LEAVE LINE BLANK FOR NEW LINE
				if( !isBlank(arrInventoryAssignment[i].line) && !isNaN(parseInt(arrInventoryAssignment[i].line)) ) {
					invDetailObj.selectLineItem('inventoryassignment', arrInventoryAssignment[i].line);
				}else{
					invDetailObj.selectNewLineItem('inventoryassignment');
					invDetailObj.setFieldValue('tolocation', inventorydetail.tolocation); 
				}
				
				if(!isBlank(arrInventoryAssignment[i].quantity)){  // total of these quantities must equal line quantity
					nlapiLogExecution('DEBUG','setBodyInvDetail', 'Setting ' + subrecord + '.inventoryassignment[' + i + '].quantity to ' + arrInventoryAssignment[i].quantity);
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'quantity', arrInventoryAssignment[i].quantity);
					nlapiLogExecution('DEBUG','setBodyInvDetail', subrecord + '.inventoryassignment[' + i + '].quantity set. ');
				}

				//Lot number or Serial number
				nlapiLogExecution('DEBUG','setBodyInvDetail', 'isBlank(arrInventoryAssignment[i].inventorynumberid) ? ' + isBlank(arrInventoryAssignment[i].inventorynumberid) + ' isBlank(arrInventoryAssignment[i].inventorynumber) ? ' + isBlank(arrInventoryAssignment[i].inventorynumber));
				if(!isBlank(arrInventoryAssignment[i].inventorynumberid) || !isBlank(arrInventoryAssignment[i].inventorynumber)){
					var inventorynumber = arrInventoryAssignment[i].inventorynumber;
					if(isBlank(inventorynumber)){
						nlapiLogExecution('DEBUG','setBodyInvDetail', subrecord + '.inventoryassignment[' + i + '].inventorynumber is blank. Looking up internalid, ' + arrInventoryAssignment[i].inventorynumberid + '.');
						inventorynumber = findRecordNameById(arrInventoryAssignment[i].inventorynumberid,'inventorynumber','inventorynumber', thisItem);
					}
					var inventorynumberId = arrInventoryAssignment[i].inventorynumberid;
					if(isBlank(inventorynumberId) && arrInventoryAssignment[i].line != 'NEW'){
						nlapiLogExecution('DEBUG','setBodyInvDetail', subrecord + '.inventoryassignment[' + i + '].inventorynumberid is blank. Looking up inventorynumber, ' + arrInventoryAssignment[i].inventorynumber + '.');
						inventorynumberId = findRecordIDbyName(arrInventoryAssignment[i].inventorynumber,'inventorynumber','inventorynumber',null,'is',thisItem); //returns an internalid or null
					}

	//JUST SET BOTH VERSIONS OF THE INVENTORY NUMBER. EACH FLIPPING RECORD TYPE USES IT DIFFERENTLY. RECEIPT IS FOR FOR CREATE NEW SERIAL NUMBER
					nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'inventorynumber = ' + inventorynumber + ' inventorynumberId = ' + inventorynumberId);
					if(inventorynumber != null){
						invDetailObj.setCurrentLineItemValue('inventoryassignment', 'inventorynumber', inventorynumber);
						invDetailObj.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', inventorynumber);
					}
					if(inventorynumberId != null && arrInventoryAssignment[i].line != 'NEW' ){
						invDetailObj.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', inventorynumberId);	
					}
				} 

				//Bin
				//FROM or CURRENT
				if(!isBlank(arrInventoryAssignment[i].binid) || !isBlank(arrInventoryAssignment[i].binnumber)){
					
					var bin = arrInventoryAssignment[i].binid;
					if(isBlank(bin)){
						if(!isBlank(arrInventoryAssignment[i].binnumber)){
							bin = findRecordIDbyName(arrInventoryAssignment[i].binnumber,'bin','binnumber',null);
						}
					}
					nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'from bin = ' + bin);
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'binnumber', bin);
				}
				//TO
				if(!isBlank(arrInventoryAssignment[i].tobinid) || !isBlank(arrInventoryAssignment[i].tobinnumber)){	
					var tobin = arrInventoryAssignment[i].tobinid;
					if(isBlank(tobin)){
						if(!isBlank(arrInventoryAssignment[i].tobinnumber)){
							tobin = findRecordIDbyName(arrInventoryAssignment[i].tobinnumber,'bin','binnumber',null);
						}
					}
					nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'to bin = ' + tobin);
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'tobinnumber', tobin);
				}
				
				//EXPIRATION DATE (for lots)
				if(isLotNumberedInventory() && !isBlank(arrInventoryAssignment[i].expirationdate)){
					invDetailObj.setCurrentLineItemValue('inventoryassignment', 'expirationdate', arrInventoryAssignment[i].expirationdate);
				}else{
					nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'BLANK EXPIRATION ' + recordtype);
					if(isLotNumberedInventory() && (recordtype == 'itemreceipt' || recordtype == 'workordercompletion' || recordtype == 'assemblybuild')){
						var thisItem = invDetailObj.getFieldValue('item');
						var itemType = findRecordTypeById(thisItem,'item');
						var itemFields = nlapiLookupField(itemType, thisItem, ['custitem_dsi_shelf_life','islotitem']);
						nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'thisItem = ' + thisItem + ' itemType = ' + itemType + ' islotitem = ' + itemFields['islotitem'] );
						if(itemFields['islotitem'] == 'T' && !isBlank(itemFields['custitem_dsi_shelf_life'])){
							var now = new Date(), then = new Date();;
							nlapiLogExecution('AUDIT', 'setBodyInvDetail', 'Auto generating expiration date ' + itemFields['custitem_dsi_shelf_life'] + ' days after today.');
							if(!isBlank(itemFields['custitem_dsi_shelf_life'])){
								then.setTime(now.getTime()+86400000*itemFields['custitem_dsi_shelf_life']); // Don't use nlapiAddDays for days over a year.
								nlapiLogExecution('DEBUG', 'setBodyInvDetail ' + subrecord, 'shelf life = ' + itemFields['custitem_dsi_shelf_life'] + ' new expiration = ' + then.toGMTString());
								invDetailObj.setCurrentLineItemValue('inventoryassignment', 'expirationdate', nlapiDateToString(then) ) ;
							}
						}
					}
				}
				nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'Committing inventoryassignment line item ' + (i+1));
				invDetailObj.commitLineItem('inventoryassignment');
			}
		}
		nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'Committing invDetailObj ' + subrecord);
		invDetailObj.commit();
	}else{
		nlapiLogExecution('DEBUG', 'setBodyInvDetail', 'inventorydetail has no data ');
	}
	return(recObj);
}

function setSubRecord(group, sub, recordObj){ // line level subrecords, usually inventorydetail
	var context = nlapiGetContext();
	var recordtype = recordObj.getRecordType();
	var location = recordObj.getFieldValue('location');
//assumes sublist line containing this subrecord is already selected-
	nlapiLogExecution('AUDIT','setSubRecord', 'BEGIN subrecord ' + sub.subrecordid + ' scriptid = ' + context.getScriptId());

	var subrecord = sub.subrecordid;
	if(subrecord == 'countdetail')
		throw nlapiCreateError("FEATURE_NOT_AVAILABLE", getErrorMessage(43), true);
	
	var viewsub = recordObj.viewCurrentLineItemSubrecord(group, sub.subrecordid);
	nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'viewsub != null? ' + (viewsub != null) );
	var subrecordObj = null;
	if(viewsub != null){
		nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'edit subrecord group = '  + group + ' subrecord = ' + sub.subrecordid);
		subrecordObj = recordObj.editCurrentLineItemSubrecord(group, sub.subrecordid);
	}else{
		nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'creating new subrecord group = '  + group + ' subrecord = ' + sub.subrecordid);
		subrecordObj = recordObj.createCurrentLineItemSubrecord(group, sub.subrecordid);
		nlapiLogExecution('DEBUG','setSubRecord', 'After Create subrecord = ' + subrecordObj);
	}	

	nlapiLogExecution('AUDIT','setSubRecord', 'subrecord = ' + JSON.stringify(subrecordObj));
	
	var hasBody = (!isBlank(sub.nvpair) && is_array(sub.nvpair) && hasData(sub.nvpair));
	var hasSublists = (!isBlank(sub.sublists) && is_array(sub.sublists) && hasData(sub.sublists));
	nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'hasBody = ' + hasBody + ' hasSublists = ' + hasSublists );
	
	if(hasBody){  //set subrecord body fields
		nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'Adding Body nvpair to subrecord ' + sub.subrecordid);
		for(var n = 0; n < sub.nvpair.length; n++){
			if(!isBlank(sub.nvpair[n])) {
				subrecordObj.setFieldValue(sub.nvpair[n].columnid, sub.nvpair[n].value);
				nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'Just set ' + sub.nvpair[n].columnid + ' to ' + sub.nvpair[n].value);
			}
		}
	}
	
	if(hasSublists){
		nlapiLogExecution('AUDIT','setSubRecord ' + subrecord, 'Begin sublists with ' + sub.sublists.length + ' sublists.');
		for(var i = 0; i< sub.sublists.length;i++){
			nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'Begin sublist ' + i + ' ' + sub.sublists[i].sublistid + ' is_array(sub.sublists[i].lines)? ' + is_array(sub.sublists[i].lines) );
			var lastSublistLine = 0, linenum = 0;
			if(viewsub != null){
				lastSublistLine = viewsub.getLineItemCount(sub.sublists[i].sublistid);
				nlapiLogExecution('DEBUG','setSubRecord', subrecord + ' already exists with ' + lastSublistLine + ' ' + sub.sublists[i].sublistid + ' lines.');
			}else{
				lastSublistLine = subrecordObj.getLineItemCount(sub.sublists[i].sublistid);
			}
			nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, '' + sub.sublists[i].sublistid + ' count = ' + subrecordObj.getLineItemCount(sub.sublists[i].sublistid) );

			// nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'lastSublistLine = ' + lastSublistLine);
			if(is_array(sub.sublists[i].lines)){
				nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'sub has ' + sub.sublists[i].lines.length + ' lines');
/***********************HERE ***********************/	
				if(recordtype == 'itemreceipt' && lastSublistLine > 0){	
					nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, ' about to validate sub.sublists[i].lines = ' + JSON.stringify(sub.sublists[i].lines));
					var correctedLines = matchNSOrder(sub.sublists[i].lines, subrecordObj, sub.sublists[i].sublistid);
					//{'subrecordObj': subrecordObj, 'lines': lines}
					subrecordObj = correctedLines.subrecordObj;
					sub.sublists[i].lines = correctedLines.lines;
					nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'lines after sort and remove = ' + JSON.stringify(sub.sublists[i].lines)  );
				}
	
				for(var j = 0; j < sub.sublists[i].lines.length; j++){
					var hasExpiration = false;
					var createnumber = sub.sublists[i].lines[j].createnumber;
					nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'Begin line ' + j + ' linenum = ' + sub.sublists[i].lines[j].linenum + ' lastSublistLine = ' + lastSublistLine );
					if((lastSublistLine == 0 && j == 0) || sub.sublists[i].lines[j].linenum == 'NEW'){  //first line of empty list or use want new line
						// append new line
						nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'New line ');
						subrecordObj.selectNewLineItem(sub.sublists[i].sublistid);
						linenum = ++lastSublistLine;
					}else{ //edit existing line
						nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'Edit line ');
						linenum = isBlank(sub.sublists[i].lines[j].linenum)?String(j+1):sub.sublists[i].lines[j].linenum;
						subrecordObj.selectLineItem(sub.sublists[i].sublistid, linenum);
					}

					nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'nvpair is array? ' + is_array(sub.sublists[i].lines[j].nvpair) );
					if(sub.sublists[i].lines[j].remove == 'T'){
						nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'Delete line ');
						subrecordObj.removeLineItem(sub.sublists[i].sublistid, linenum, false);
						lastSublistLine--;
					}else if(is_array(sub.sublists[i].lines[j].nvpair)){
						nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'nvpair.length = ' + sub.sublists[i].lines[j].nvpair.length );
						nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'nvpair is array? ' + is_array(sub.sublists[i].lines[j].nvpair) );						
						for(var k = 0; k < sub.sublists[i].lines[j].nvpair.length; k++){
							nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'subrecordObj = ' + JSON.stringify(subrecordObj) + ' k = ' + k);
							
							if(sub.sublists[i].lines[j].nvpair[k].columnid == 'inventorynumber'){
								var inventorynumber = sub.sublists[i].lines[j].nvpair[k].value;
								if(!isBlank(inventorynumber)){
									if(createnumber == 'T'){
										inventorynumberid = null;
										inventorynumber = sub.sublists[i].lines[j].nvpair[k].value;
										nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'FORCE NEW LOT/SERIAL inventorynumber = ' + inventorynumber + ' inventorynumberid = null');
									}else{
										var inventorynumberid = inventorynumber;
										//var thisItem = isItemItem(recordObj) || recordtype == 'bintransfer' ? subrecordObj.getFieldValue('item'):null;
										var thisItem = subrecordObj.getFieldValue('item');
										var tempNum = findRecordIDbyName(inventorynumber,'inventorynumber','inventorynumber',null,'is',thisItem); //returns an internalid or null
											nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'tempNum = ' + tempNum + ' ' + ' is null? ' +  (tempNum == null) + ' thisItem = ' + thisItem);
										var tempName = findRecordNameById(inventorynumber,'inventorynumber','inventorynumber',thisItem);
											nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'tempName = ' + tempName + ' ' + ' is null? ' + (tempName == null) + ' thisItem = ' + thisItem);			
										if(tempNum){ // inventorynumber value was the name
											inventorynumberid = tempNum;
										}else if(tempName){  // inventorynumber value was the internalid (or invalid, that throws error later)
											inventorynumber = tempName;
										}else { // if both null, put it back to create a new one or so error message makes sense
											inventorynumber = sub.sublists[i].lines[j].nvpair[k].value; 
											inventorynumberid = null;
											nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'NEW LOT/SERIAL inventorynumber = ' + inventorynumber + ' inventorynumberid = null');
										}
									}										
//										if(inventorynumberid){
//											var invNumRec = nlapiLoadRecord('inventorynumber', inventorynumberid);
//											nlapiLogExecution('AUDIT','setSubRecord', 'invNumRec = ' + JSON.stringify(invNumRec));
//										}
										nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'setting inventorynumber to ' + inventorynumber);
									subrecordObj.setCurrentLineItemValue(sub.sublists[i].sublistid, 'inventorynumber', inventorynumber);
										// nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'get value inventorynumber is ' + subrecordObj.getCurrentLineItemValue(sub.sublists[i].sublistid, 'inventorynumber'));
									
										nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'setting issueinventorynumber to ' + inventorynumberid );
									subrecordObj.setCurrentLineItemValue(sub.sublists[i].sublistid, 'issueinventorynumber', inventorynumberid);
										// nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'get value issueinventorynumber is ' + subrecordObj.getCurrentLineItemValue(sub.sublists[i].sublistid, 'issueinventorynumber'));
										
										nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'setting receiptinventorynumber to ' + inventorynumber);
									subrecordObj.setCurrentLineItemValue(sub.sublists[i].sublistid, 'receiptinventorynumber', inventorynumber);
										// nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'get value receiptinventorynumber is ' + subrecordObj.getCurrentLineItemValue(sub.sublists[i].sublistid, 'receiptinventorynumber'));
								}else{
									nlapiLogExecution('AUDIT','setSubRecord ' + subrecord, 'inventory nvpair with blank value');
								}
							} else if(sub.sublists[i].lines[j].nvpair[k].columnid == 'binnumber'){
								var binnumber = sub.sublists[i].lines[j].nvpair[k].value;
								var tempBin = findRecordIDbyName(binnumber,'bin','binnumber',null,'is', null, location);
								if(tempBin != null)  binnumber = tempBin;
								nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'setting binnumber to ' + binnumber);
								subrecordObj.setCurrentLineItemValue(sub.sublists[i].sublistid, 'binnumber', binnumber);
							}else if(sub.sublists[i].lines[j].nvpair[k].columnid == 'tobinnumber'){
								var tobinnumber = sub.sublists[i].lines[j].nvpair[k].value;
								var tempBin = findRecordIDbyName(tobinnumber,'bin','binnumber',null,'is', null, location);
								if(tempBin != null)  tobinnumber = tempBin;
								nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'setting tobinnumber to ' + tobinnumber);
								subrecordObj.setCurrentLineItemValue(sub.sublists[i].sublistid, 'tobinnumber', tobinnumber);
							} else { // not inventorynumber or a bin
								nlapiLogExecution('DEBUG','setSubRecord ' + subrecord, 'subrecordObj.setCurrentLineItemValue("' + sub.sublists[i].sublistid + '", "' + sub.sublists[i].lines[j].nvpair[k].columnid + '", "' + sub.sublists[i].lines[j].nvpair[k].value + '")');
								if(isLotNumberedInventory() && sub.sublists[i].lines[j].nvpair[k].columnid == 'expirationdate'){
									sub.sublists[i].lines[j].nvpair[k].value = nlapiDateToString(new Date(sub.sublists[i].lines[j].nvpair[k].value));
									hasExpiration = true;
								}
								subrecordObj.setCurrentLineItemValue(sub.sublists[i].sublistid, sub.sublists[i].lines[j].nvpair[k].columnid, sub.sublists[i].lines[j].nvpair[k].value);
								nlapiLogExecution('AUDIT','setSubRecord ' + subrecord, 'after setting inventorydetail, now get value = ' + subrecordObj.getCurrentLineItemValue(sub.sublists[i].sublistid, sub.sublists[i].lines[j].nvpair[k].columnid) );
							}
						}
						if(isLotNumberedInventory() && !hasExpiration){
							//AUTO EXPIRATION 
							nlapiLogExecution('DEBUG', 'setSubRecord', 'BLANK EXPIRATION SECTION');
							//var recordtype = recordObj.getRecordType();
							//nlapiLogExecution('DEBUG', 'setSubRecord', 'recordtype = ' + recordtype + ' subrecord = ' + subrecord);
							if(subrecord.indexOf('inventorydetail') == 0 && (recordtype == 'itemreceipt' || recordtype == 'workordercompletion' || recordtype == 'assemblybuild'))  // && tempNum == null
							{   
								var thisItem = subrecordObj.getFieldValue('item');
								var itemType = findRecordTypeById(thisItem,'item');
								var itemFields = nlapiLookupField(itemType, thisItem, ['custitem_dsi_shelf_life','islotitem']);
								//nlapiLogExecution('DEBUG', 'setSubRecord', 'itemFields = ' + JSON.stringify(itemFields));
								//nlapiLogExecution('DEBUG', 'setSubRecord', 'thisItem = ' + thisItem + ' itemType = ' + itemType + ' islotitem = ' + itemFields['islotitem'] + ' shelf life = ' + itemFields['custitem_dsi_shelf_life'] + ' blank? ' + isBlank(itemFields['custitem_dsi_shelf_life']));
								if(itemFields['islotitem'] == 'T' && !isBlank(itemFields['custitem_dsi_shelf_life']) ){
									var now = new Date(), then = new Date();
									nlapiLogExecution('AUDIT', 'setSubRecord', 'Auto generating expiration date ' + itemFields['custitem_dsi_shelf_life'] + ' days after today.');
									if(!isBlank(itemFields['custitem_dsi_shelf_life'])){
										then.setTime(now.getTime()+86400000*itemFields['custitem_dsi_shelf_life']);
										nlapiLogExecution('DEBUG', 'setSubRecord ' + subrecord, 'shelf life = ' + itemFields['custitem_dsi_shelf_life'] + ' new expiration = ' + then.toGMTString());
										subrecordObj.setCurrentLineItemValue('inventoryassignment', 'expirationdate', nlapiDateToString(then) ) ;
									}
								}
							}
						}
						nlapiLogExecution('DEBUG', 'setSubRecord ' + subrecord, 'Committing ' + sub.sublists[i].sublistid + ' line item ' + linenum );
						subrecordObj.commitLineItem(sub.sublists[i].sublistid);
					}

				}
			}
		}
	}
	if(hasSublists || hasBody){
		nlapiLogExecution('DEBUG', 'setSubRecord', 'subrecordObj = ' + JSON.stringify(subrecordObj));
		nlapiLogExecution('DEBUG', 'setSubRecord', 'Committing subrecordObj ' + sub.subrecordid);
		subrecordObj.commit();
	}else{
		//fail silently
		nlapiLogExecution('AUDIT', 'setSubRecord', 'Nothing to do. hasSublists and hasBody both false');
	}
	nlapiLogExecution('DEBUG', 'setSubRecord', 'END');
	return(recordObj);
}



function matchNSOrder(lines, subrecordObj, sublistid){
	nlapiLogExecution('DEBUG', 'matchNSOrder BEGIN', 'lines = ' + JSON.stringify(lines) + ' sublistid = ' + sublistid);
	nlapiLogExecution('DEBUG', 'matchNSOrder BEGIN', 'subrecordObj = ' + JSON.stringify(subrecordObj) );
	subrecordObj = removeUnwantedLines(lines, subrecordObj, sublistid);
	nlapiLogExecution('DEBUG', 'matchNSOrder AFTER REMOVE', 'subrecordObj = ' + JSON.stringify(subrecordObj) );
	// intended for transfer order receipt where quantity must match
	// removeUnwantedLines
	// reorder lines array to the same as subrecorrdObj after unwanted lines are removed
	var count = subrecordObj.getLineItemCount(sublistid);
	for(var j = 0; j < lines.length; j++){
		nlapiLogExecution('DEBUG', 'matchNSOrder', 'j = ' + j + ' lines[j] = ' + JSON.stringify(lines[j]) );
		var number = getNvpairValue(lines[j].nvpair, 'inventorynumber');
		var qty = getNvpairValue(lines[j].nvpair, 'quantity');

		for(var i = 1; i <= count; i++){
			nlapiLogExecution('DEBUG', 'matchNSOrder','i = ' + i + ' j = ' + j + ' count = ' + count);
			var thisNumberVal = subrecordObj.getLineItemValue(sublistid, 'receiptinventorynumber', i) || '';
			var thisNumberText = subrecordObj.getLineItemText(sublistid, 'receiptinventorynumber', i) || '';
			var thisQuantity = subrecordObj.getLineItemValue(sublistid, 'quantity', i) || '';

			//if( (number == thisNumberVal || number == thisNumberText) && qty == thisQuantity){
				if(number == thisNumberVal || number == thisNumberText){
				nlapiLogExecution('DEBUG', 'matchNSOrder','FOUND changing linenum to ' + i);
				lines[j].linenum = String(i);
			}
		}
		nlapiLogExecution('DEBUG', 'matchNSOrder', 'After inner loop lines = ' + JSON.stringify(lines) );
	}

	nlapiLogExecution('DEBUG', 'matchNSOrder', 'Before sort lines = ' + JSON.stringify(lines) );
	//sort ascending by linenum number
	lines.sort(function(a,b) {return (parseInt(a.linenum) < parseInt(b.linenum)) ? -1 : ((parseInt(b.linenum) < parseInt(a.linenum)) ? 1 : 0);} ); 
	return {'subrecordObj': subrecordObj, 'lines': lines};
}

function getNvpairValue(arr, column){
	//nlapiLogExecution('DEBUG', 'getNvpairValue', 'column = ' + column +  ' arr = ' + JSON.stringify(arr) );
	var retval = '';
	for(var x = 0; x < arr.length; x++){
		if(arr[x].columnid == column){
			return arr[x].value;
		}
	}
	return retval;
}

function removeUnwantedLines(lines, subrecordObj, sublistid){
	nlapiLogExecution('DEBUG', 'removeUnwantedLines BEGIN', 'lines = ' + JSON.stringify(lines) + ' sublistid = ' + sublistid);
	nlapiLogExecution('DEBUG', 'removeUnwantedLines BEGIN', 'subrecordObj = ' + JSON.stringify(subrecordObj) );

	//backwards to remove from end, because line number changes on remove.
	for(var i = subrecordObj.getLineItemCount(sublistid); i > 0; i--){
		var found = false;
		var thisNumberVal = subrecordObj.getLineItemValue(sublistid, 'receiptinventorynumber', i) || '';
		var thisNumberText = subrecordObj.getLineItemText(sublistid, 'receiptinventorynumber', i) || '';
		var thisQuantity = subrecordObj.getLineItemValue(sublistid, 'quantity', i) || '';
		nlapiLogExecution('DEBUG', 'removeUnwantedLines', sublistid + ' line = ' + i +  ' thisNumberText = ' + thisNumberText + ' thisNumberVal = ' + thisNumberVal + ' thisQuantity = ' + thisQuantity );
		for(var j = 0; j < lines.length; j++){
			var number = getNvpairValue(lines[j].nvpair, 'inventorynumber');
			var qty = getNvpairValue(lines[j].nvpair, 'quantity');
			nlapiLogExecution('DEBUG', 'removeUnwantedLines', 'request line ' + j + ' number = ' + number + ' qty = ' + qty );
			nlapiLogExecution('DEBUG', 'removeUnwantedLines', 'befoe if, number == thisNumberVal? ' + (number == thisNumberVal) );
			//if( (number == thisNumberVal || number == thisNumberText) && qty == thisQuantity)
			if( number == thisNumberVal || number == thisNumberText )
			{
				nlapiLogExecution('DEBUG', 'removeUnwantedLines', 'found match on request line ' + j);
				found = true;
				break; // stop loop when found to improve performance
			}
		}
		if(!found){
			nlapiLogExecution('DEBUG', 'removeUnwantedLines', 'match not found. removing sublist line ' + i);
			subrecordObj.removeLineItem(sublistid, i, false);
		}
	}
	return subrecordObj
}

function setBodySubRecord(sub, recordObj){ // body level subrecords, usually inventorydetail
	nlapiLogExecution('AUDIT','setBodySubRecord', 'BEGIN');
	var context = nlapiGetContext();
	var recordtype = recordObj.getRecordType();

	nlapiLogExecution('AUDIT','setBodySubRecord', 'BEGIN subrecord ' + sub.subrecordid + ' scriptid = ' + context.getScriptId());
	var subrecord = sub.subrecordid;
	if(isBlank(subrecord) ) subrecord = 'inventorydetail'; //default to inventorydetail
	if(subrecord == 'countdetail')
		throw nlapiCreateError("FEATURE_NOT_AVAILABLE", getErrorMessage(43), true);	
	
	
	var viewsub = recordObj.viewSubrecord(sub.subrecordid);
	nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'viewsub != null? ' + (viewsub != null) );
	var subrecordObj = null;
	if(viewsub != null){
		nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'edit subrecord = ' + sub.subrecordid);
		subrecordObj = recordObj.editSubrecord(sub.subrecordid);
	}else{
		nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'creating new subrecord = ' + sub.subrecordid);
		subrecordObj = recordObj.createSubrecord(sub.subrecordid);
	}	

	var hasBody = (is_array(sub.nvpair) && hasData(sub.nvpair));
	var hasSublists = (is_array(sub.sublists) && hasData(sub.sublists));
	nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'hasBody = ' + hasBody + ' hasSublists = ' + hasSublists );
	
	if(hasBody){  //set subrecord body fields
		nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'Adding Body nvpair to subrecord ' + sub.subrecordid);
		for(var n = 0; n < sub.nvpair.length; n++){
			if(!isBlank(sub.nvpair[n])) {
				subrecordObj.setFieldValue(sub.nvpair[n].columnid, sub.nvpair[n].value);
				nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'Just set ' + sub.nvpair[n].columnid + ' to ' + sub.nvpair[n].value);
			}
		}
	}
	
	if(hasSublists){
		nlapiLogExecution('AUDIT','setBodySubRecord ' + subrecord, 'Begin sublists with ' + sub.sublists.length + ' sublists.');
		
		for(var i = 0; i< sub.sublists.length;i++){
			var sublistid = sub.sublists[i].sublistid;
			if(isBlank(sublistid) ) sublistid = 'inventoryassignment'; //default to inventoryassignment
			nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'Begin sublist ' + i + ' ' + sublistid + ' is_array(sub.sublists[i].lines)? ' + is_array(sub.sublists[i].lines) );
			var lastSublistLine = 0, linenum = 0;
			if(viewsub != null){
				lastSublistLine = viewsub.getLineItemCount(sublistid);
				nlapiLogExecution('DEBUG','setBodySubRecord', subrecord + ' already exists with ' + lastSublistLine + ' ' + sublistid + ' lines.');
			}
			if(is_array(sub.sublists[i].lines)){
				var hasExpiration = false;
				for(var j = 0; j < sub.sublists[i].lines.length; j++){
					nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'Begin line ' + j + ' linenum = ' + sub.sublists[i].lines[j].linenum + ' lastSublistLine = ' + lastSublistLine );
					var createnumber = sub.sublists[i].lines[j].createnumber;
					if(lastSublistLine == 0 || sub.sublists[i].lines[j].linenum.toUpperCase() == 'NEW'){
						// append new line
						nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'New line ');
						subrecordObj.selectNewLineItem(sublistid);
						linenum = ++lastSublistLine;
					}else{ //edit existing line
						nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'Edit line ');
						linenum = sub.sublists[i].lines[j].linenum;
						subrecordObj.selectLineItem(sublistid, linenum);
					}

					nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'nvpair is array? ' + is_array(sub.sublists[i].lines[j].nvpair) );
					if(sub.sublists[i].lines[j].remove == 'T'){
						nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'Delete line ');
						subrecordObj.removeLineItem(sublistid, linenum, false);
						lastSublistLine--;
					}else if(is_array(sub.sublists[i].lines[j].nvpair)){
						nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'nvpair.length = ' + sub.sublists[i].lines[j].nvpair.length );
						for(var k = 0; k < sub.sublists[i].lines[j].nvpair.length; k++){
							nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'subrecordObj = ' + subrecordObj + ' k = ' + k);
							var thisItem = subrecordObj.getFieldValue('item');
							if(sub.sublists[i].lines[j].nvpair[k].columnid == 'inventorynumber'){
								var inventorynumber = sub.sublists[i].lines[j].nvpair[k].value;
								var inventorynumberid = (createnumber == 'T')? null: inventorynumber;
									var tempNum = findRecordIDbyName(inventorynumber,'inventorynumber','inventorynumber',null,'is',thisItem); //returns an internalid or null
									nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'tempNum = ' + tempNum + ' ' + (tempNum == null));
									var tempName = findRecordNameById(inventorynumber,'inventorynumber','inventorynumber',thisItem);
									nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'tempName = ' + tempName + ' ' + (tempName == null));
								if(createnumber == 'T'){
									inventorynumberid = null;
									nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'FORCE NEW LOT/SERIAL inventorynumber = ' + inventorynumber + ' inventorynumberid = null');
								}else{									
									if(tempNum){ // inventorynumber value was the name
										inventorynumberid = tempNum;
									}else if(tempName){  // inventorynumber value was the internalid (or invalid, that throws error later)
										inventorynumber = tempName;
									}else { // if both null, put it back to create a new one or so error message makes sense
										inventorynumber = sub.sublists[i].lines[j].nvpair[k].value; 
										inventorynumberid = null;
									}
								}
									
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'setting inventorynumber to ' + inventorynumberid);
								subrecordObj.setCurrentLineItemValue(sublistid, 'inventorynumber', inventorynumberid);
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'inventorynumber get value = ' + subrecordObj.getCurrentLineItemValue(sublistid, 'inventorynumber') );
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'setting issueinventorynumber to ' + inventorynumberid );
								subrecordObj.setCurrentLineItemValue(sublistid, 'issueinventorynumber', inventorynumberid);
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'issueinventorynumber get value = ' + subrecordObj.getCurrentLineItemValue(sublistid, 'issueinventorynumber') );

								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'setting receiptinventorynumber to ' + inventorynumber);
								subrecordObj.setCurrentLineItemValue(sublistid, 'receiptinventorynumber', inventorynumber);
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'receiptinventorynumber get value = ' + subrecordObj.getCurrentLineItemValue(sublistid, 'receiptinventorynumber') );
							} else if(sub.sublists[i].lines[j].nvpair[k].columnid == 'binnumber'){
								var binnumber = sub.sublists[i].lines[j].nvpair[k].value;
								var tempBin = findRecordIDbyName(binnumber,'bin','binnumber',null);
								if(tempBin != null)  binnumber = tempBin;
								
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'setting binnumber to ' + binnumber);
								subrecordObj.setCurrentLineItemValue(sublistid, 'binnumber', binnumber);
							}else if(sub.sublists[i].lines[j].nvpair[k].columnid == 'tobinnumber'){
								var tobinnumber = sub.sublists[i].lines[j].nvpair[k].value;
								var tempBin = findRecordIDbyName(tobinnumber,'bin','binnumber',null);
								if(tempBin != null)  binnumber = tempBin;

								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'setting tobinnumber to ' + tobinnumber);
								subrecordObj.setCurrentLineItemValue(sublistid, 'tobinnumber', tobinnumber);
							}  else { // not invnentorynumber or a bin
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'subrecordObj.setCurrentLineItemValue("' + sublistid + '", "' + sub.sublists[i].lines[j].nvpair[k].columnid + '", "' + sub.sublists[i].lines[j].nvpair[k].value + '")');
								subrecordObj.setCurrentLineItemValue(sublistid, sub.sublists[i].lines[j].nvpair[k].columnid, sub.sublists[i].lines[j].nvpair[k].value);
								 if(isLotNumberedInventory() && sub.sublists[i].lines[j].nvpair[k].columnid == 'expirationdate'){
									hasExpiration = true;
								}
								nlapiLogExecution('DEBUG','setBodySubRecord ' + subrecord, 'get value = ' + subrecordObj.getCurrentLineItemValue(sublistid, sub.sublists[i].lines[j].nvpair[k].columnid) );
							}
							
							
						}
						if(isLotNumberedInventory() && !hasExpiration){
							//AUTO EXPIRATION 
							nlapiLogExecution('DEBUG', 'setBodySubRecord', 'BLANK EXPIRATION');
							var recordtype = recordObj.getRecordType();
							nlapiLogExecution('DEBUG', 'setBodySubRecord', 'recordtype = ' + recordtype + ' subrecord = ' + subrecord);
							if(~subrecord.indexOf('inventorydetail') && (recordtype == 'itemreceipt' || recordtype == 'workordercompletion' || recordtype == 'assemblybuild')) // && tempNum == null
							{
								var thisItem = subrecordObj.getFieldValue('item');
								var itemType = findRecordTypeById(thisItem,'item');
								var itemFields = nlapiLookupField(itemType, thisItem, ['custitem_dsi_shelf_life','islotitem']);
								nlapiLogExecution('DEBUG', 'setBodySubRecord', 'thisItem = ' + thisItem + ' itemType = ' + itemType + ' islotitem = ' + itemFields['islotitem'] );
								if(itemFields['islotitem'] == 'T' && !isBlank(itemFields['custitem_dsi_shelf_life'])){
									var now = new Date(), then = new Date();
									nlapiLogExecution('AUDIT', 'setBodySubRecord', 'Auto generating expiration date ' + itemFields['custitem_dsi_shelf_life'] + ' days after today.');
									if(!isBlank(itemFields['custitem_dsi_shelf_life'])){
										then.setTime(now.getTime()+86400000*itemFields['custitem_dsi_shelf_life']); //Don't use nlapiAddDays for days over a year.
										nlapiLogExecution('DEBUG', 'setBodySubRecord ' + subrecord, 'shelf life = ' + itemFields['custitem_dsi_shelf_life'] + ' new expiration = ' + then.toGMTString());
										subrecordObj.setCurrentLineItemValue('inventoryassignment', 'expirationdate', nlapiDateToString(then) ) ;
									}
								}
							}
						}
						nlapiLogExecution('DEBUG', 'setBodySubRecord ' + subrecord, 'Committing ' + sublistid + ' line item ' + linenum );
						subrecordObj.commitLineItem(sublistid);
					}

				}
			}
		}
	}
	if(hasSublists || hasBody){
		nlapiLogExecution('DEBUG', 'setBodySubRecord', 'Committing subrecordObj ' + sub.subrecordid);
		subrecordObj.commit();
	}else{
		//fail silently
		nlapiLogExecution('DEBUG', 'setBodySubRecord', 'Nothing to do ');
	}
	
	return(recordObj);
}


function itemDetailSearch(options){ 
/*	This function requires the Advanced Bin/Numbered Management feature enabled.
 *	Takes one parameter, an options object: {itemNumber:"itemid/name", itemId:"item internalid", locationId:"location internalid", islotitem:boolean, isserialitem:boolean, usebins:boolean, filteronhand:boolean}
 *
 *	Required fields: Either itemNumber or itemID. 
 *					 locationId.  
 *
 *	Optional fields: All default to false.
 *					islotitem
 *					isserialitem 
 *					usebins 
 *					filteronhand: true to filter by quantityonhand > 0, false to filter by quantityavailable > 0. 
 * 
 *	Returns: An array with one or more objects. 
 *		each detail object is {binnumber:'', quantityavailable:'', quantityonhand:'', inventorynumber:'', expirationdate:''}
 *		if required fields missing or usebins, islotitem, and isserialitem are all false, returns array with one empty object
 */ 
	nlapiLogExecution('DEBUG', 'itemDetailSearch', 'BEGIN options = ' + JSON.stringify(options));
	var retVal = []; 
	var missingItem = (isBlank(options.itemNumber) && isBlank(options.itemId));
	var allFalseBools = (!options.islotitem && !options.isserialitem && !options.usebins);
	if(missingItem || isBlank(options.locationId) || allFalseBools){
		retVal.push(new detailObj());
	}else{
		var join = null;
		var hasNumber = (options.islotitem || options.isserialitem);
		if( options.usebins && hasNumber){
			nlapiLogExecution('AUDIT', 'itemDetailSearch', 'Bin and Number');
			join = 'inventorynumberbinonhand';
		}else if(options.usebins && !hasNumber){
			nlapiLogExecution('AUDIT', 'itemDetailSearch', 'Bin, no number');
			join = 'binonhand';
		}else if(!options.usebins && hasNumber){
			nlapiLogExecution('AUDIT', 'itemDetailSearch', 'Number, no bin');
			join = 'inventoryNumber';
		}else{//no details needed, return empty object
			return(new detailObj());
		}
		nlapiLogExecution('DEBUG', 'itemDetailSearch', 'join =  ' + join);
		
		var filters = [], columns = [];
		if(options.itemId){
			filters.push(new nlobjSearchFilter('internalid',null,'anyof',options.itemId.trim()));
		}else{
			filters.push(new nlobjSearchFilter('itemid',null,'is',options.itemNumber.trim()));
		}
		filters.push(new nlobjSearchFilter('location',join,'anyof',options.locationId));
		if(options.filteronhand){
			filters.push(new nlobjSearchFilter('quantityonhand',join,'greaterthan','0'));
		}else{
			filters.push(new nlobjSearchFilter('quantityavailable',join,'greaterthan','0'));
		}
		
		columns.push(new nlobjSearchColumn('quantityavailable',join).setSort(false));
		columns.push(new nlobjSearchColumn('quantityonhand',join));
		
		if(options.usebins) columns.push(new nlobjSearchColumn('binnumber',join).setSort(false));
		if(options.islotitem || options.isserialitem) {
			columns.push(new nlobjSearchColumn('inventorynumber',join));
		}
		if(isLotNumberedInventory() && join == 'inventoryNumber' && options.islotitem){
			columns.push(new nlobjSearchColumn('expirationdate',join));
		}

		var results = nlapiSearchRecord("item",null,filters, columns);
		nlapiLogExecution('DEBUG', 'itemDetailSearch', 'results is null? ' + (results == null));
		if(results){
			nlapiLogExecution('DEBUG', 'itemDetailSearch', 'results.length = ' + results.length);
			for(var i = 0; i < results.length; i++){
				var row = results[i];
				var thisRow = new detailObj();
				thisRow.quantityavailable = row.getValue('quantityavailable',join);
				thisRow.quantityonhand = row.getValue('quantityonhand',join);
				thisRow.binid = (options.usebins) ? row.getValue('binnumber',join) : '';
				thisRow.binnumber = (options.usebins) ? row.getText('binnumber',join) : '';
// DO NOT CHANGE THIS AGAIN, THE CODE BELOW DOES NOT WORK
				thisRow.inventorynumber = (options.islotitem) 
				? (join == 'inventoryNumber' ? row.getValue('inventorynumber',join) : row.getText('inventorynumber',join)) 
				: (options.isserialitem ? (options.usebins ? row.getText('inventorynumber',join) : row.getValue('inventorynumber', join)) : '');
// THIS DOES NOT WORK
				//thisRow.inventorynumber = (options.islotitem) ? row.getText('inventorynumber',join) : (options.isserialitem ? row.getValue('inventorynumber',join) : '');
//				thisRow.inventorynumber = (options.islotitem || options.isserialitem ? (join == 'inventoryNumber' ? row.getText('inventorynumber',join) : row.getText('inventorynumber',join)) : '');

				nlapiLogExecution('DEBUG', 'itemDetailSearch', 'row.getValue(\'inventorynumber\',' + join + ')');
				if(isLotNumberedInventory() && join == 'inventoryNumber'){
					thisRow.expirationdate = row.getValue('expirationdate',join);
				}else{
					thisRow.expirationdate = (isLotNumberedInventory() && options.islotitem) ? getExpirationDate(thisRow.inventorynumber,row.getId()) : '';
				}
				retVal.push(thisRow);
			}
		}
	}
	nlapiLogExecution('DEBUG', 'itemDetailSearch', 'retVal = ' + JSON.stringify(retVal));
	options.data = retVal;  // this is here so you can call this function with a larger object and get values by reference (ask Chris)
	return retVal;
	
	function detailObj(){ //private object
		this.binnumber='';
		this.binid='';
		this.quantityavailable='';
		this.quantityonhand='';
		this.inventorynumber='';
		this.expirationdate='';
		this.quantityinput = '';
	}
}

function getExpirationDate(inventorynumber,id){
//	nlapiLogExecution('DEBUG', 'getExpirationDate', 'BEGIN inventorynumber = ' + inventorynumber + ' (item) id = ' + id);
	var retVal = '';
	var filters = [], columns = [];
	filters.push(new nlobjSearchFilter('inventorynumber',null,'is',inventorynumber));
	filters.push(new nlobjSearchFilter('item',null,'anyof',id));
	columns.push(new nlobjSearchColumn('expirationdate'));
	var results = nlapiSearchRecord("inventorynumber",null,filters, columns);
	if(results){
		//retVal = results[0].getValue(columns[0]); // all results should be identical, only take the first one
		retVal = results[0].getText(columns[0]);
	}

	return retVal;
}
